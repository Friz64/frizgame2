#!/bin/sh
set -e # exit on errors
cd $(dirname $0)

./shadercomp.sh

RUST_BACKTRACE=1 cargo run --features "dev bevy/dynamic"
