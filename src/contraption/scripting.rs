use super::Contraption;
use crate::{model::RenderedBundle, AssetHandles};
use arrayvec::ArrayVec;
use bevy::{core::FixedTimestep, ecs::query::QueryEntityError, prelude::*, utils::HashMap};
use bevy_rapier3d::prelude::*;
use egui::{
    epaint::text::cursor::RCursor,
    text::LayoutJob,
    text_edit::{CCursorRange, CursorRange},
};
use erupt::SmallVec;
use rhai::{Dynamic, Engine, EvalAltResult, FnPtr, ParseError, Position, Scope, AST};
use std::{
    cell::RefCell,
    collections::VecDeque,
    mem,
    rc::Rc,
    time::{Duration, Instant},
};

const MAX_PARTS_PER_SCRIPT: usize = 50;
const DEFAULT_SCRIPT: &str = "\
let cube = cube(vec3splat(0.0));
|| {
\tlet dir = (vec3(0.0, 1.5, 0.0) - cube.pos);
\tcube.impulse(dir / 400.0);
}
";

mod native {
    use super::*;

    #[derive(Clone)]
    pub struct Context {
        pub id: i64,
    }

    pub type Part = Rc<RefCell<PartInner>>;

    pub struct PartInner {
        pub pos: Vec3,
        pub impulse: Vec3,
    }

    pub fn cube(pos: Vec3) -> Part {
        Rc::new(RefCell::new(PartInner {
            pos,
            impulse: Vec3::ZERO,
        }))
    }
}

pub struct ScriptingPlugin;

impl Plugin for ScriptingPlugin {
    fn build(&self, app: &mut App) {
        app.insert_non_send_resource(ScriptingCtx::new())
            .add_stage(
                RunScriptsStage,
                SystemStage::parallel().with_run_criteria(FixedTimestep::step(1.0 / 60.0)),
            )
            .add_system_to_stage(RunScriptsStage, run_scripts);
    }
}

struct LogEntry {
    pos: Position,
    text: String,
    count: usize,
    timestamp: Duration,
}

impl LogEntry {
    pub fn started() -> LogEntry {
        LogEntry {
            pos: Position::NONE,
            text: "Logging started".into(),
            count: 1,
            timestamp: Duration::ZERO,
        }
    }
}

struct Log(VecDeque<LogEntry>);

impl Default for Log {
    fn default() -> Self {
        let mut entries = VecDeque::new();
        entries.push_back(LogEntry::started());
        Log(entries)
    }
}

impl Log {
    fn append(&mut self, start: &Instant, pos: Position, text: String) {
        if self.0.len() >= 1000 {
            self.0.pop_front();
        }

        match self.0.back_mut() {
            Some(last) if last.text == text && last.pos == pos => last.count += 1,
            _ => self.0.push_back(LogEntry {
                pos,
                text,
                count: 1,
                timestamp: start.elapsed(),
            }),
        }
    }
}

#[derive(Debug)]
struct ActiveScript {
    ast: AST,
    scope: Scope<'static>,
    update_fn: FnPtr,
    start: Instant,
}

#[derive(Debug)]
enum PendingScript {
    Error { pos: Position, text: String },
    Compiled { ast: AST },
    None,
}

struct ContraptionScripts {
    active: Option<ActiveScript>,
    pending: PendingScript,
    log: Log,
    start: Instant,
    spawn_pos: Vec3,
    tracked_parts: ArrayVec<(native::Part, Entity), MAX_PARTS_PER_SCRIPT>,
}

impl ContraptionScripts {
    fn new(spawn_pos: Vec3) -> ContraptionScripts {
        ContraptionScripts {
            active: None,
            pending: PendingScript::None,
            log: Default::default(),
            start: Instant::now(),
            spawn_pos,
            tracked_parts: Default::default(),
        }
    }
}

impl ContraptionScripts {
    fn compile(&mut self, engine: &Engine, script: &str) {
        self.pending = match engine.compile(&script) {
            Ok(ast) => PendingScript::Compiled { ast },
            Err(ParseError(inner, pos)) => {
                let text = inner.to_string();
                PendingScript::Error { pos, text }
            }
        };
    }
}

pub struct ScriptingCtx {
    engine: Engine,
    scripts: Rc<RefCell<HashMap<Entity, ContraptionScripts>>>,
    running: Rc<RefCell<Entity>>,
}

impl ScriptingCtx {
    fn new() -> ScriptingCtx {
        let mut ctx = ScriptingCtx {
            engine: Engine::new(),
            scripts: Rc::new(RefCell::new(HashMap::default())),
            running: Rc::new(RefCell::new(Entity::from_raw(u32::MAX))),
        };

        ctx.engine.disable_symbol("eval");
        ctx.engine.disable_symbol("print");
        ctx.engine.disable_symbol("debug");
        ctx.engine.set_max_operations(50000);
        ctx.engine.set_max_string_size(500);
        ctx.engine.set_max_array_size(500);
        ctx.engine.set_max_map_size(500);
        ctx.engine.set_max_expr_depths(0, 0);

        ctx.engine
            .register_custom_syntax(&["log", "$expr$"], false, {
                let scripts = ctx.scripts.clone();
                let running = ctx.running.clone();
                move |ctx, inputs| {
                    assert_eq!(inputs.len(), 1);
                    let log_expr = &inputs[0];
                    let pos = log_expr.position();
                    let value = ctx.eval_expression_tree(log_expr)?;

                    let text = if value.is::<Vec3>() {
                        format!("{:#?}", value.cast::<Vec3>())
                    } else {
                        value.to_string()
                    };

                    let mut all_scripts = scripts.borrow_mut();
                    let running = all_scripts.get_mut(&running.borrow()).unwrap();
                    running.log.append(&running.start, pos, text);

                    Ok(Dynamic::UNIT)
                }
            })
            .unwrap();

        ctx.engine
            .register_type::<Vec3>()
            .register_fn("vec3", Vec3::new)
            .register_fn("vec3splat", Vec3::splat)
            .register_fn("+", |a: Vec3, b: Vec3| a + b)
            .register_fn("-", |a: Vec3, b: Vec3| a - b)
            .register_fn("*", |a: Vec3, b: Vec3| a * b)
            .register_fn("/", |a: Vec3, b: Vec3| a / b)
            .register_fn("+", |a: Vec3, b: f32| a + b)
            .register_fn("-", |a: Vec3, b: f32| a - b)
            .register_fn("*", |a: Vec3, b: f32| a * b)
            .register_fn("/", |a: Vec3, b: f32| a / b)
            .register_get_set("x", |v: &mut Vec3| v.x, |v: &mut Vec3, x: f32| v.x = x)
            .register_get_set("y", |v: &mut Vec3| v.y, |v: &mut Vec3, y: f32| v.y = y)
            .register_get_set("z", |v: &mut Vec3| v.z, |v: &mut Vec3, z: f32| v.z = z);

        ctx.engine
            .register_type::<native::Part>()
            .register_result_fn("cube", {
                let scripts = ctx.scripts.clone();
                let running = ctx.running.clone();
                move |offset: Vec3| {
                    let mut all_scripts = scripts.borrow_mut();
                    let running = all_scripts.get_mut(&running.borrow()).unwrap();

                    let cube = native::cube(running.spawn_pos + offset);
                    running
                        .tracked_parts
                        .try_push((cube.clone(), Entity::from_raw(u32::MAX)))
                        .map_err(|_| "maximum part number reached")?;

                    Ok(cube)
                }
            })
            .register_get("pos", |part: &mut native::Part| part.borrow().pos)
            .register_fn("impulse", |part: &mut native::Part, impulse: Vec3| {
                part.borrow_mut().impulse += impulse;
            });

        ctx.engine
            .register_type::<native::Context>()
            .register_get("id", |ctx: &mut native::Context| ctx.id);

        ctx
    }

    fn scripts<T>(&self, entity: Entity, f: impl FnOnce(&mut ContraptionScripts) -> T) -> T {
        let mut all_scripts = self.scripts.borrow_mut();
        f(all_scripts.get_mut(&entity).unwrap())
    }
}

#[derive(Debug, Component)]
pub struct Scripted {
    script: String,
    dirty: bool,
    apply: bool,
    set_cursor: Option<CCursorRange>,
    scroll_to: Option<egui::Response>,
}

impl Default for Scripted {
    fn default() -> Self {
        Self {
            script: DEFAULT_SCRIPT.into(),
            dirty: true,
            apply: true,
            set_cursor: None,
            scroll_to: None,
        }
    }
}

impl Scripted {
    pub fn from_script(script: String) -> Scripted {
        Scripted {
            script,
            dirty: true,
            apply: true,
            set_cursor: None,
            scroll_to: None,
        }
    }

    pub fn script(&self) -> String {
        self.script.clone()
    }

    pub fn reset(&mut self) {
        self.dirty = true;
        self.apply = true;
    }
}

#[derive(Clone, Hash, Debug, PartialEq, Eq, StageLabel)]
struct RunScriptsStage;

fn take_err_pos_recursive(err: &mut EvalAltResult) -> Position {
    if let EvalAltResult::ErrorInFunctionCall(_name, _src, inner, _pos) = err {
        take_err_pos_recursive(inner)
    } else {
        err.take_position()
    }
}

fn run_scripts(
    mut commands: Commands,
    asset_handles: Res<AssetHandles>,
    ctx: NonSend<ScriptingCtx>,
    mut metadata: Query<(Entity, &Contraption, &Transform, &mut Scripted)>,
    mut data: Query<(
        &Transform,
        &RigidBodyMassPropsComponent,
        &mut RigidBodyVelocityComponent,
    )>,
) {
    {
        let mut all_scripts = ctx.scripts.borrow_mut();

        // add newly added
        for (entity, _contraption, spawn, _scripted) in metadata.iter_mut() {
            all_scripts
                .entry(entity)
                .or_insert_with(|| ContraptionScripts::new(spawn.translation));
        }

        // remove despawned
        let entities: Vec<_> = all_scripts.keys().cloned().collect();
        for entity in entities {
            if matches!(metadata.get(entity), Err(QueryEntityError::NoSuchEntity)) {
                let scripts = all_scripts.remove(&entity).unwrap();
                for (_part, entity) in scripts.tracked_parts {
                    commands.entity(entity).despawn();
                }
            }
        }
    }

    for (entity, contraption, spawn, mut scripted) in metadata.iter_mut() {
        *ctx.running.borrow_mut() = entity;

        let ctx_native = native::Context {
            id: contraption.id as i64,
        };

        let mut new_ast = None;
        ctx.scripts(entity, |scripts| {
            if scripted.dirty {
                scripts.compile(&ctx.engine, &scripted.script);
            }

            if scripted.apply {
                if let PendingScript::Compiled { ast } = &mut scripts.pending {
                    new_ast = Some(mem::take(ast));
                }
            }
        });

        let mut active_updated = false;
        if let Some(ast) = new_ast {
            let mut scope = Scope::new();
            scope.push("ctx", ctx_native.clone());

            let old_scripts = ctx.scripts(entity, |scripts| {
                mem::replace(scripts, ContraptionScripts::new(spawn.translation))
            });

            match ctx.engine.eval_ast_with_scope::<FnPtr>(&mut scope, &ast) {
                Ok(update_fn) => {
                    for (_part, entity) in old_scripts.tracked_parts {
                        commands.entity(entity).despawn();
                    }

                    active_updated = true;
                    ctx.scripts(entity, |scripts| {
                        scripted.dirty = false;
                        scripted.apply = false;
                        scripts.pending = PendingScript::None;
                        scripts.active = Some(ActiveScript {
                            ast,
                            scope,
                            update_fn,
                            start: scripts.start,
                        });
                    });
                }
                Err(mut err) => {
                    ctx.scripts(entity, |scripts| {
                        *scripts = old_scripts;

                        let pos = take_err_pos_recursive(&mut err);
                        let text = err.to_string();
                        scripts.pending = PendingScript::Error { pos, text };
                    });
                }
            };
        }

        if !active_updated {
            ctx.scripts(entity, |scripts| {
                scripts.spawn_pos = spawn.translation;

                for (part, entity) in &scripts.tracked_parts {
                    let mut part = part.borrow_mut();
                    let (transform, _mass_props, _vel) = data.get_mut(*entity).unwrap();
                    part.pos = transform.translation;
                }

                if let Some(active) = &mut scripts.active {
                    *active.scope.get_mut("ctx").unwrap().write_lock().unwrap() = ctx_native;
                }
            });
        }
    }

    for (entity, _contraption, _spawn, _scripted) in metadata.iter() {
        *ctx.running.borrow_mut() = entity;

        let active_script = ctx.scripts(entity, |scripts| mem::take(&mut scripts.active));
        if let Some(active) = active_script {
            let res: Result<(), _> = active.update_fn.call(&ctx.engine, &active.ast, ());
            ctx.scripts(entity, |scripts| {
                if let Err(mut err) = res {
                    let pos = take_err_pos_recursive(&mut err);
                    let text = err.to_string();

                    scripts.log.append(&active.start, pos, text);
                }

                scripts.active = Some(active);
            });
        }
    }

    let mut despawned = SmallVec::new();
    for (entity, _contraption, _spawn, _scripted) in metadata.iter_mut() {
        ctx.scripts(entity, |scripts| {
            despawned.clear();
            for (i, (part, entity)) in scripts.tracked_parts.iter().enumerate() {
                if Rc::strong_count(part) == 1 {
                    despawned.push(i);
                    if entity.id() != u32::MAX {
                        commands.entity(*entity).despawn();
                    }
                }
            }

            for i in despawned.iter().rev() {
                scripts.tracked_parts.swap_remove(*i);
            }

            for (part, entity) in &scripts.tracked_parts {
                if entity.id() != u32::MAX {
                    let mut part = part.borrow_mut();
                    if part.impulse.is_finite() && part.impulse.length_squared() > 1e-6 {
                        let (_transform, mass_props, mut vel) = data.get_mut(*entity).unwrap();
                        vel.apply_impulse(mass_props, part.impulse.into());
                        part.impulse = Vec3::ZERO;
                    }
                }
            }

            for (part, entity) in &mut scripts.tracked_parts {
                let part = part.borrow();
                if entity.id() == u32::MAX {
                    let size = Vec3::ONE * 0.2;
                    *entity = commands
                        .spawn()
                        .insert_bundle(RigidBodyBundle {
                            position: part.pos.into(),
                            ..Default::default()
                        })
                        .insert_bundle(ColliderBundle {
                            shape: ColliderShape::cuboid(size.x, size.y, size.z).into(),
                            mass_properties: ColliderMassProps::Density(0.5).into(),
                            ..Default::default()
                        })
                        .insert(RigidBodyPositionSync::Interpolated { prev_pos: None })
                        .insert_bundle(RenderedBundle {
                            model: asset_handles.cube.clone(),
                            transform: Transform::from_scale(size),
                            ..Default::default()
                        })
                        .id();
                }
            }
        });
    }
}

fn row_column(pos: Position) -> Option<(usize, usize)> {
    let line = pos.line();
    let position = pos.position();
    line.and_then(|line| position.map(|pos| (line - 1, pos - 1)))
}

pub fn ui(
    ui: &mut egui::Ui,
    entity: Entity,
    scripting_ctx: &ScriptingCtx,
    scripted: &mut Scripted,
) {
    let mut all_scripts = scripting_ctx.scripts.borrow_mut();
    let scripts = match all_scripts.get_mut(&entity) {
        Some(scripts) => scripts,
        None => return,
    };

    let mut jump = None;
    let apply_btn = ui.add_enabled(scripted.dirty, egui::Button::new("Apply edits"));

    if let Some(scroll_to) = scripted.scroll_to.take() {
        scroll_to.scroll_to_me(Some(egui::Align::Center));
    }

    let mut editor = egui::ScrollArea::both()
        .id_source(ui.id().with("input"))
        .max_height(300.0)
        .show_viewport(ui, |ui, scroll_visible| {
            let mut layouter = |ui: &egui::Ui, string: &str, _wrap_width: f32| {
                ui.fonts().layout_job(LayoutJob::simple(
                    string.into(),
                    egui::TextStyle::Monospace.resolve(ui.style()),
                    ui.visuals().widgets.inactive.text_color(),
                    f32::INFINITY,
                ))
            };

            let mut editor = egui::TextEdit::multiline(&mut scripted.script)
                .layouter(&mut layouter)
                .lock_focus(true)
                .desired_width(f32::INFINITY)
                .show(ui);

            if let Some(set_cursor) = scripted.set_cursor.take() {
                editor.state.set_ccursor_range(Some(set_cursor));
            }

            if editor.response.changed() {
                scripted.dirty = true;
            }

            match &mut scripts.pending {
                PendingScript::Error { pos, text } => {
                    let mut popup_pos = ui
                        .clip_rect()
                        .left_bottom()
                        .min(editor.response.rect.left_bottom());

                    let row_column = row_column(*pos);
                    let mut popup_offscreen = true;
                    if let Some((row, column)) = row_column {
                        if let Some(row) = editor.galley.rows.get(row) {
                            let err_offset = egui::vec2(row.x_offset(column), row.max_y() + 4.0);
                            if scroll_visible.contains(err_offset.to_pos2()) {
                                popup_pos = editor.response.rect.min + err_offset;
                                popup_offscreen = false;
                            }
                        }
                    }

                    egui::Area::new(ui.id().with("error_tooltip"))
                        .order(egui::Order::Foreground)
                        .fixed_pos(popup_pos)
                        .show(ui.ctx(), |ui| {
                            let frame = egui::Frame::popup(ui.style()).multiply_with_opacity(0.8);
                            frame.show(ui, |ui| {
                                ui.horizontal(|ui| {
                                    if popup_offscreen {
                                        let jump_btn = ui.add_enabled(
                                            row_column.is_some(),
                                            egui::Button::new("Jump"),
                                        );

                                        if jump_btn.clicked() {
                                            jump = row_column;
                                        }
                                    }

                                    ui.label(egui::RichText::new(&*text).color(egui::Color32::RED));
                                });
                            });
                        });
                }
                PendingScript::Compiled { .. } => {
                    if scripted.dirty
                        && (apply_btn.clicked() || {
                            editor.response.has_focus() && {
                                let input = ui.input();
                                input.modifiers.ctrl && input.key_down(egui::Key::Space)
                            }
                        })
                    {
                        scripted.apply = true;
                    }
                }
                PendingScript::None => (),
            }

            editor
        })
        .inner;

    ui.separator();

    egui::ScrollArea::vertical()
        .id_source(ui.id().with("log"))
        .stick_to_bottom()
        .show(ui, |ui| {
            ui.set_min_height(50.0);
            ui.set_width(ui.available_width());
            for entry in scripts.log.0.iter() {
                ui.horizontal(|ui| {
                    let row_column = row_column(entry.pos);
                    let jump_btn = ui.add_enabled(
                        !scripted.dirty && row_column.is_some(),
                        egui::Button::new("Jump"),
                    );

                    if jump_btn.clicked() {
                        jump = row_column;
                    }

                    ui.small(format!("{:.02?}", entry.timestamp));

                    if entry.count > 1 {
                        ui.small(format!("({})", entry.count));
                    }

                    ui.add(egui::Label::new(&entry.text).wrap(true));
                });
            }
        });

    if let Some((row, column)) = jump {
        let cursor = editor.galley.from_rcursor(RCursor { row, column });
        let cursor_range = CursorRange::one(cursor);
        editor.state.set_cursor_range(Some(cursor_range));
        ui.memory().request_focus(editor.response.id);
        editor.state.store(ui.ctx(), editor.response.id);

        let row = &editor.galley.rows[row];
        let mut row_response = editor.response.clone();
        row_response.rect = row.rect.translate(editor.response.rect.min.to_vec2());
        scripted.scroll_to = Some(row_response);
    }

    apply_btn.on_hover_text("Ctrl+Space while focused");
}
