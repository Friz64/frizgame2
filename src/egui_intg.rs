use crate::{vulkan::RenderStage, Grabbed, UpdateGrabbed};
use bevy::{
    input::{
        keyboard::KeyboardInput,
        mouse::{MouseScrollUnit, MouseWheel},
        ElementState,
    },
    prelude::*,
};
use copypasta::{ClipboardContext, ClipboardProvider};

pub struct EguiPlugin;

impl Plugin for EguiPlugin {
    fn build(&self, app: &mut App) {
        app.init_non_send_resource::<EguiCtx>()
            .init_resource::<EguiRenderData>()
            .init_resource::<InputLocked>()
            .add_system_to_stage(CoreStage::PreUpdate, begin_frame.after(UpdateGrabbed))
            .add_system_to_stage(RenderStage::Prepare, end_frame);
    }
}

#[derive(Default)]
pub struct InputLocked(pub bool);

pub struct EguiCtx {
    pub ctx: egui::Context,
    clipboard: ClipboardContext,
    cursor_pos: egui::Pos2,
    cursor_icon: CursorIcon,
    focused_last_frame: bool,
}

impl Default for EguiCtx {
    fn default() -> Self {
        let ctx = egui::Context::default();
        ctx.set_visuals(egui::Visuals::dark());

        EguiCtx {
            ctx,
            clipboard: ClipboardContext::new().expect("failed to get clipboard context"),
            cursor_pos: egui::Pos2::default(),
            cursor_icon: CursorIcon::Default,
            focused_last_frame: true,
        }
    }
}

#[allow(clippy::too_many_arguments)]
fn begin_frame(
    mut egui: NonSendMut<EguiCtx>,
    mut input_locked: ResMut<InputLocked>,
    windows: Res<Windows>,
    time: Res<Time>,
    grabbed: Res<Grabbed>,
    keyboard_input: Res<Input<KeyCode>>,
    mouse_button_input: Res<Input<MouseButton>>,
    mut ev_cursor_moved: EventReader<CursorMoved>,
    mut ev_cursor_left: EventReader<CursorLeft>,
    mut ev_mouse_wheel: EventReader<MouseWheel>,
    mut ev_keyboard_input: EventReader<KeyboardInput>,
    mut ev_received_text_input: EventReader<ReceivedTextInput>,
) {
    let mut raw_input = egui::RawInput::default();

    let window = windows.get_primary().unwrap();
    let screen_size = egui::pos2(window.width(), window.height());
    raw_input.screen_rect = Some(egui::Rect {
        min: egui::Pos2::ZERO,
        max: screen_size,
    });

    raw_input.pixels_per_point = Some(window.scale_factor() as f32);
    raw_input.predicted_dt = time.delta_seconds();

    let pressed = |input| keyboard_input.pressed(input);
    let alt = pressed(KeyCode::AltLeft) || pressed(KeyCode::AltRight);
    let ctrl = pressed(KeyCode::ControlLeft) || pressed(KeyCode::ControlRight);
    let shift = pressed(KeyCode::ShiftLeft) || pressed(KeyCode::ShiftRight);
    raw_input.modifiers = egui::Modifiers {
        alt,
        ctrl,
        shift,
        mac_cmd: false,
        command: ctrl,
    };

    raw_input.events.clear();

    if !grabbed.0 {
        for cursor_moved in ev_cursor_moved.iter() {
            let position = cursor_moved.position;
            let cursor_pos = egui::pos2(position.x, screen_size.y - position.y);

            raw_input.events.push(egui::Event::PointerMoved(cursor_pos));
            egui.cursor_pos = cursor_pos;
        }

        let mut mouse_btn = |bevy_btn, egui_btn| {
            raw_input.events.push(egui::Event::PointerButton {
                pos: egui::pos2(egui.cursor_pos.x, egui.cursor_pos.y),
                button: egui_btn,
                pressed: if mouse_button_input.just_pressed(bevy_btn) {
                    true
                } else if mouse_button_input.just_released(bevy_btn) {
                    false
                } else {
                    return;
                },
                modifiers: raw_input.modifiers,
            });
        };

        mouse_btn(MouseButton::Left, egui::PointerButton::Primary);
        mouse_btn(MouseButton::Middle, egui::PointerButton::Middle);
        mouse_btn(MouseButton::Right, egui::PointerButton::Secondary);

        for _cursor_left in ev_cursor_left.iter() {
            raw_input.events.push(egui::Event::PointerGone);
        }

        for mouse_wheel in ev_mouse_wheel.iter() {
            let mut delta = egui::vec2(mouse_wheel.x, mouse_wheel.y);
            if let MouseScrollUnit::Line = mouse_wheel.unit {
                // https://github.com/emilk/egui/blob/b1fd6a44e82c8cde7fab0b952851d2d2fd785349/egui-winit/src/lib.rs#L449
                delta *= 50.0;
            }

            raw_input.events.push(egui::Event::Scroll(delta));
        }
    }

    if window.is_focused() == egui.focused_last_frame {
        for keyboard_input in ev_keyboard_input.iter() {
            if ctrl && keyboard_input.state == ElementState::Pressed {
                match keyboard_input.key_code {
                    KeyCode::KeyX => raw_input.events.push(egui::Event::Cut),
                    KeyCode::KeyC => raw_input.events.push(egui::Event::Copy),
                    KeyCode::KeyV => match egui.clipboard.get_contents() {
                        Ok(contents) => raw_input.events.push(egui::Event::Text(contents)),
                        Err(err) => warn!("Failed to get clipboard text: {err}"),
                    },
                    _ => (),
                }
            }

            let key = match keyboard_input.key_code {
                KeyCode::ArrowDown => egui::Key::ArrowDown,
                KeyCode::ArrowLeft => egui::Key::ArrowLeft,
                KeyCode::ArrowRight => egui::Key::ArrowRight,
                KeyCode::ArrowUp => egui::Key::ArrowUp,
                KeyCode::Escape => egui::Key::Escape,
                KeyCode::Tab => egui::Key::Tab,
                KeyCode::Backspace => egui::Key::Backspace,
                KeyCode::Enter => egui::Key::Enter,
                KeyCode::Space => egui::Key::Space,
                KeyCode::Insert => egui::Key::Insert,
                KeyCode::Delete => egui::Key::Delete,
                KeyCode::Home => egui::Key::Home,
                KeyCode::End => egui::Key::End,
                KeyCode::PageUp => egui::Key::PageUp,
                KeyCode::PageDown => egui::Key::PageDown,
                KeyCode::Digit0 | KeyCode::Numpad0 => egui::Key::Num0,
                KeyCode::Digit1 | KeyCode::Numpad1 => egui::Key::Num1,
                KeyCode::Digit2 | KeyCode::Numpad2 => egui::Key::Num2,
                KeyCode::Digit3 | KeyCode::Numpad3 => egui::Key::Num3,
                KeyCode::Digit4 | KeyCode::Numpad4 => egui::Key::Num4,
                KeyCode::Digit5 | KeyCode::Numpad5 => egui::Key::Num5,
                KeyCode::Digit6 | KeyCode::Numpad6 => egui::Key::Num6,
                KeyCode::Digit7 | KeyCode::Numpad7 => egui::Key::Num7,
                KeyCode::Digit8 | KeyCode::Numpad8 => egui::Key::Num8,
                KeyCode::Digit9 | KeyCode::Numpad9 => egui::Key::Num9,
                KeyCode::KeyA => egui::Key::A,
                KeyCode::KeyB => egui::Key::B,
                KeyCode::KeyC => egui::Key::C,
                KeyCode::KeyD => egui::Key::D,
                KeyCode::KeyE => egui::Key::E,
                KeyCode::KeyF => egui::Key::F,
                KeyCode::KeyG => egui::Key::G,
                KeyCode::KeyH => egui::Key::H,
                KeyCode::KeyI => egui::Key::I,
                KeyCode::KeyJ => egui::Key::J,
                KeyCode::KeyK => egui::Key::K,
                KeyCode::KeyL => egui::Key::L,
                KeyCode::KeyM => egui::Key::M,
                KeyCode::KeyN => egui::Key::N,
                KeyCode::KeyO => egui::Key::O,
                KeyCode::KeyP => egui::Key::P,
                KeyCode::KeyQ => egui::Key::Q,
                KeyCode::KeyR => egui::Key::R,
                KeyCode::KeyS => egui::Key::S,
                KeyCode::KeyT => egui::Key::T,
                KeyCode::KeyU => egui::Key::U,
                KeyCode::KeyV => egui::Key::V,
                KeyCode::KeyW => egui::Key::W,
                KeyCode::KeyX => egui::Key::X,
                KeyCode::KeyY => egui::Key::Y,
                KeyCode::KeyZ => egui::Key::Z,
                _ => continue,
            };

            raw_input.events.push(egui::Event::Key {
                key,
                pressed: keyboard_input.state == ElementState::Pressed,
                modifiers: raw_input.modifiers,
            });
        }

        if !ctrl {
            for received_text_input in ev_received_text_input.iter() {
                let text: String = received_text_input
                    .text
                    .chars()
                    .filter(|c| c.is_ascii_graphic() || *c == ' ' || !c.is_ascii())
                    .collect();

                if !text.is_empty() {
                    raw_input.events.push(egui::Event::Text(text));
                }
            }
        }
    }

    egui.ctx.begin_frame(raw_input);
    input_locked.0 = egui.ctx.wants_keyboard_input();
    egui.focused_last_frame = window.is_focused();
}

#[derive(Default)]
pub struct EguiRenderData {
    pub primitives: Vec<egui::ClippedPrimitive>,
    pub textures_delta: egui::TexturesDelta,
}

fn end_frame(
    mut egui: NonSendMut<EguiCtx>,
    mut windows: ResMut<Windows>,
    mut render_data: ResMut<EguiRenderData>,
) {
    let output = egui.ctx.end_frame();
    let platform_output = output.platform_output;

    let window = windows.get_primary_mut().unwrap();
    let cursor_icon = match platform_output.cursor_icon {
        egui::CursorIcon::Default | egui::CursorIcon::None => CursorIcon::Default,
        egui::CursorIcon::ContextMenu => CursorIcon::ContextMenu,
        egui::CursorIcon::Help => CursorIcon::Help,
        egui::CursorIcon::PointingHand => CursorIcon::Hand,
        egui::CursorIcon::Progress => CursorIcon::Progress,
        egui::CursorIcon::Wait => CursorIcon::Wait,
        egui::CursorIcon::Cell => CursorIcon::Cell,
        egui::CursorIcon::Crosshair => CursorIcon::Crosshair,
        egui::CursorIcon::Text => CursorIcon::Text,
        egui::CursorIcon::VerticalText => CursorIcon::VerticalText,
        egui::CursorIcon::Alias => CursorIcon::Alias,
        egui::CursorIcon::Copy => CursorIcon::Copy,
        egui::CursorIcon::Move => CursorIcon::Move,
        egui::CursorIcon::NoDrop => CursorIcon::NoDrop,
        egui::CursorIcon::NotAllowed => CursorIcon::NotAllowed,
        egui::CursorIcon::Grab => CursorIcon::Grab,
        egui::CursorIcon::Grabbing => CursorIcon::Grabbing,
        egui::CursorIcon::AllScroll => CursorIcon::AllScroll,
        egui::CursorIcon::ResizeHorizontal => CursorIcon::ColResize,
        egui::CursorIcon::ResizeNeSw => CursorIcon::NeswResize,
        egui::CursorIcon::ResizeNwSe => CursorIcon::NwseResize,
        egui::CursorIcon::ResizeVertical => CursorIcon::RowResize,
        egui::CursorIcon::ZoomIn => CursorIcon::ZoomIn,
        egui::CursorIcon::ZoomOut => CursorIcon::ZoomOut,
    };

    if cursor_icon != egui.cursor_icon {
        egui.cursor_icon = cursor_icon;
        window.set_cursor_icon(cursor_icon);
    }

    if !platform_output.copied_text.is_empty() {
        if let Err(err) = egui.clipboard.set_contents(platform_output.copied_text) {
            warn!("Failed to set clipboard text: {err}");
        }
    }

    render_data.primitives = egui.ctx.tessellate(output.shapes);
    render_data.textures_delta = output.textures_delta;
}
