pub mod asset_interface;
mod ctx;
mod passes;
mod swapchain;
mod transfer;
mod utils;

use self::{
    ctx::{VulkanCtx, VulkanCtxContainer},
    passes::Passes,
    swapchain::{Frame, Swapchain},
};
use crate::{
    config::Config, egui_intg::EguiRenderData, image::Image, model::Model,
    vulkan::asset_interface::AssetInterface, UP_VEC,
};
use bevy::{app::AppExit, asset::AssetStage, prelude::*, winit::WinitWindows};
use erupt::vk;
use std::time::{Duration, Instant};
use winit::window::Window as WinitWindow;

pub const FRAMES_IN_FLIGHT: usize = 2;

#[derive(Default)]
pub struct RenderPlugin;

#[derive(Debug, Hash, PartialEq, Eq, Clone, StageLabel)]
pub enum RenderStage {
    Prepare,
    Render,
}

impl Plugin for RenderPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<LightPos>()
            .add_startup_system(setup_vulkan.exclusive_system().label(SetupVulkanSystem))
            .add_stage_after(
                AssetStage::AssetEvents,
                RenderStage::Prepare,
                SystemStage::parallel(),
            )
            .add_stage_after(
                RenderStage::Prepare,
                RenderStage::Render,
                SystemStage::parallel(),
            )
            .add_system_to_stage(RenderStage::Render, render);
    }
}

#[derive(Default)]
pub struct LightPos(pub Vec3);

pub struct SkyboxImage(pub Handle<Image>);

pub struct CameraPos {
    pub pos: Vec3,
    pub direction: Vec3,
}

impl CameraPos {
    pub fn view_projection_matrices(
        &self,
        fov: f32,
        width: f32,
        height: f32,
        flip_y: bool,
    ) -> (Mat4, Mat4) {
        let view = Mat4::look_at_rh(self.pos, self.pos + self.direction, UP_VEC);

        let aspect_ratio = width / height;
        let mut projection =
            Mat4::perspective_infinite_reverse_rh(fov.to_radians(), aspect_ratio, 0.001);
        if flip_y {
            projection.y_axis.y *= -1.0; // flip y for vulkan
        }

        (view, projection)
    }
}

#[derive(Clone, Hash, Debug, PartialEq, Eq, SystemLabel)]
pub struct SetupVulkanSystem;

fn setup_vulkan(world: &mut World) {
    let windows = world.get_resource::<Windows>().unwrap();
    let winit_windows = world.get_resource::<WinitWindows>().unwrap();
    let window = windows.get_primary().unwrap();
    let winit_window = winit_windows.get_window(window.id()).unwrap();

    let ctx = unsafe { VulkanCtx::new(winit_window) };

    let renderer = unsafe { Renderer::new(ctx.clone()) };
    world.insert_non_send_resource(renderer);

    let asset_interface = AssetInterface::new(ctx);
    world.insert_resource(asset_interface);
}

pub struct RenderData<'a> {
    skybox: Option<Res<'a, SkyboxImage>>,
    scale_factor: f64,
    egui: &'a EguiRenderData,
    models: Vec<(&'a Handle<Model>, &'a GlobalTransform)>,
    light_pos: Vec3,
    cam_pos: &'a CameraPos,
    frame_rate_limit: f32,
    fov: f32,
}

#[allow(clippy::too_many_arguments)]
fn render(
    mut vulkan: NonSendMut<Renderer>,
    mut app_exit_events: EventReader<AppExit>,
    asset_interface: Res<AssetInterface>,
    windows: Res<Windows>,
    winit_windows: Res<WinitWindows>,
    skybox: Option<Res<SkyboxImage>>,
    egui_render_data: Res<EguiRenderData>,
    model_query: Query<(&Handle<Model>, &GlobalTransform), With<Transform>>,
    light_pos: Res<LightPos>,
    cam_pos: Res<CameraPos>,
    config: Res<Config>,
) {
    if app_exit_events.iter().next().is_some() {
        // destroy swapchain early
        // https://gitlab.freedesktop.org/mesa/mesa/-/issues/176
        unsafe {
            let ctx = vulkan.ctx.get();
            ctx.device.device_wait_idle().unwrap();
            vulkan.swapchain.destroy(&ctx);
        }

        return;
    }

    let window = windows.get_primary().unwrap();
    let winit_window = winit_windows.get_window(window.id()).unwrap();

    let render_data = RenderData {
        skybox,
        scale_factor: window.scale_factor(),
        egui: &egui_render_data,
        models: model_query.iter().collect(),
        light_pos: light_pos.0,
        cam_pos: &cam_pos,
        frame_rate_limit: config.frame_rate_limit,
        fov: config.fov,
    };

    unsafe { vulkan.render(&asset_interface, window, winit_window, &render_data) };
}

#[derive(Clone, Copy)]
pub struct ImageWithView {
    handle: vk::Image,
    view: vk::ImageView,
}

pub struct Renderer {
    ctx: VulkanCtxContainer,
    frame_idx: usize,
    swapchain: Swapchain,
    swapchain_frame: Option<Frame>,
    passes: Passes,
    frame_start: Instant,
}

impl Renderer {
    unsafe fn new(ctx_container: VulkanCtxContainer) -> Renderer {
        let ctx = ctx_container.get();
        let swapchain = Swapchain::new(&ctx);
        let passes = Passes::new(&ctx, swapchain.format_pair());
        drop(ctx);

        Renderer {
            ctx: ctx_container,
            frame_idx: 0,
            swapchain,
            swapchain_frame: None,
            passes,
            frame_start: Instant::now(),
        }
    }

    unsafe fn acquire_frame(&mut self, window: &Window, winit_window: &WinitWindow) {
        if self.swapchain_frame.is_none() {
            let ctx = self.ctx.get();
            let frame = self
                .swapchain
                .acquire_frame(&ctx, self.frame_idx, window, winit_window);
            self.swapchain_frame = Some(frame);
        }
    }

    unsafe fn render(
        &mut self,
        asset_interface: &AssetInterface,
        window: &Window,
        winit_window: &WinitWindow,
        render_data: &RenderData,
    ) {
        self.acquire_frame(window, winit_window);

        let ctx = self.ctx.get();
        let frame = self.swapchain_frame.take().unwrap();
        asset_interface.acquire_barrier(frame.cmd_buf());
        self.passes.render(&ctx, &frame, render_data);
        self.swapchain.present_frame(&ctx, frame);
        self.frame_idx = (self.frame_idx + 1) % FRAMES_IN_FLIGHT;
        drop(ctx);

        if render_data.frame_rate_limit.is_finite() {
            let limit = Duration::from_secs_f32(1.0 / render_data.frame_rate_limit);
            spin_sleep::sleep(limit.saturating_sub(self.frame_start.elapsed()));
        }

        self.frame_start = Instant::now();

        if self.swapchain.present_mode() == vk::PresentModeKHR::FIFO_KHR {
            self.acquire_frame(window, winit_window);
        }
    }
}

impl Drop for Renderer {
    fn drop(&mut self) {
        unsafe {
            let ctx = self.ctx.get();
            ctx.device.device_wait_idle().unwrap();
            self.passes.destroy(&ctx);
            drop(ctx);

            self.ctx.destroy();
            info!("Successfully deinitialized the renderer");
        }
    }
}
