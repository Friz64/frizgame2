use crate::{config::Config, egui_intg::EguiCtx, vulkan::CameraPos};
use bevy::prelude::*;
use egui_gizmo::GizmoMode;

pub struct GizmoPlugin;

impl Plugin for GizmoPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(ui);
    }
}

#[derive(Component)]
pub struct Gizmo(pub GizmoMode);

fn ui(
    egui: NonSend<EguiCtx>,
    config: Res<Config>,
    camera_pos: Res<CameraPos>,
    mut query: Query<(Entity, &Gizmo, &mut Transform)>,
) {
    egui::Area::new("gizmo")
        .fixed_pos((0.0, 0.0))
        .show(&egui.ctx, |ui| {
            ui.with_layer_id(egui::LayerId::background(), |ui| {
                for (entity, gizmo, mut transform) in query.iter_mut() {
                    let model = transform.compute_matrix();
                    let screen = ui.input().screen_rect();
                    let (view, projection) = camera_pos.view_projection_matrices(
                        config.fov,
                        screen.width(),
                        screen.height(),
                        false,
                    );

                    if let Some(response) = egui_gizmo::Gizmo::new(ui.id().with(entity))
                        .model_matrix(model.to_cols_array_2d())
                        .view_matrix(view.to_cols_array_2d())
                        .projection_matrix(projection.to_cols_array_2d())
                        .mode(gizmo.0)
                        .interact(ui)
                    {
                        let new_model = Mat4::from_cols_array_2d(&response.transform);
                        *transform = Transform::from_matrix(new_model);
                    }
                }
            })
        });
}
