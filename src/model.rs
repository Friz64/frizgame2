use crate::vulkan::{asset_interface::AssetInterface, SetupVulkanSystem};
use bevy::{
    asset::{AssetLoader, AssetPath, BoxedFuture, LoadContext, LoadedAsset},
    prelude::*,
    reflect::TypeUuid,
};

pub struct ModelPlugin;

impl Plugin for ModelPlugin {
    fn build(&self, app: &mut App) {
        app.add_asset::<Model>()
            .add_startup_system(ObjLoader::setup.exclusive_system().after(SetupVulkanSystem));
    }
}

#[derive(Clone, TypeUuid)]
#[uuid = "0a5de7b7-e295-4fb6-93d5-42ba963f3610"]
pub struct Model;

#[derive(Default, Debug, Clone, Bundle)]
pub struct RenderedBundle {
    pub model: Handle<Model>,
    pub transform: Transform,
    pub global_transform: GlobalTransform,
}

#[repr(C)]
#[derive(Clone, Copy)]
pub struct Vertex {
    pub pos: Vec3,
    pub normal: Vec3,
}

unsafe impl bytemuck::Pod for Vertex {}
unsafe impl bytemuck::Zeroable for Vertex {}

pub struct ModelDesc {
    pub vertices: Vec<Vertex>,
    pub indices: Vec<u32>,
}

pub struct ObjLoader(AssetInterface);

impl ObjLoader {
    fn setup(asset_server: ResMut<AssetServer>, intf: Res<AssetInterface>) {
        asset_server.add_loader(ObjLoader(intf.clone()));
    }
}

impl AssetLoader for ObjLoader {
    fn load<'a>(
        &'a self,
        mut bytes: &'a [u8],
        load_context: &'a mut LoadContext,
    ) -> BoxedFuture<'a, Result<(), anyhow::Error>> {
        Box::pin(async move {
            let (mut models, _materials) = tobj::load_obj_buf(
                &mut bytes,
                &tobj::LoadOptions {
                    triangulate: true,
                    ignore_points: true,
                    ignore_lines: true,
                    single_index: true,
                },
                |_| unimplemented!(),
            )?;

            assert_eq!(models.len(), 1);
            let model = models.remove(0).mesh;

            let vertices: Vec<Vertex> = model
                .positions
                .chunks_exact(3)
                .into_iter()
                .zip(model.normals.chunks_exact(3))
                .map(|(pos, normal)| Vertex {
                    pos: <[f32; 3]>::try_from(pos).unwrap().into(),
                    normal: <[f32; 3]>::try_from(normal).unwrap().into(),
                })
                .collect();

            let desc = ModelDesc {
                vertices,
                indices: model.indices,
            };

            unsafe {
                self.0
                    .upload_model(AssetPath::from(load_context.path()).get_id().into(), desc);
            }

            load_context.set_default_asset(LoadedAsset::new(Model));
            Ok(())
        })
    }

    fn extensions(&self) -> &[&str] {
        &["obj"]
    }
}
