use crate::vulkan::{asset_interface::AssetInterface, SetupVulkanSystem};
use bevy::{
    asset::{AssetLoader, AssetPath, BoxedFuture, LoadContext, LoadedAsset},
    prelude::*,
    reflect::TypeUuid,
};
use ktx2::SupercompressionScheme;
use std::borrow::Cow;

pub struct ImagePlugin;

impl Plugin for ImagePlugin {
    fn build(&self, app: &mut App) {
        app.add_asset::<Image>().add_startup_system(
            Ktx2Loader::setup
                .exclusive_system()
                .after(SetupVulkanSystem),
        );
    }
}

#[derive(Clone, TypeUuid)]
#[uuid = "1e8ad8d7-dfef-4bcd-950c-481116bff17d"]
pub struct Image;

pub struct Ktx2Loader(AssetInterface);

impl Ktx2Loader {
    fn setup(asset_server: ResMut<AssetServer>, intf: Res<AssetInterface>) {
        asset_server.add_loader(Ktx2Loader(intf.clone()));
    }
}

impl AssetLoader for Ktx2Loader {
    fn load<'a>(
        &'a self,
        bytes: &'a [u8],
        load_context: &'a mut LoadContext,
    ) -> BoxedFuture<'a, Result<(), anyhow::Error>> {
        Box::pin(async move {
            let reader = ktx2::Reader::new(bytes).expect("Failed to decode KTX2");
            let header = reader.header();

            let levels = reader
                .levels()
                .map(|data| match header.supercompression_scheme {
                    Some(SupercompressionScheme::Zstandard) => {
                        let data = zstd::decode_all(data)
                            .expect("failed to decompress Zstandard KTX2 texture data");
                        Cow::Owned(data)
                    }
                    Some(other) => unimplemented!("KTX2 {other:?} supercompression"),
                    None => Cow::Borrowed(data),
                })
                .collect();

            let handle = AssetPath::from(load_context.path()).get_id().into();
            unsafe { self.0.upload_ktx2(handle, header, levels) };

            load_context.set_default_asset(LoadedAsset::new(Image));
            Ok(())
        })
    }

    fn extensions(&self) -> &[&str] {
        &["ktx2"]
    }
}
