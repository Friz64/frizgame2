mod config;
mod contraption;
mod controller;
mod debug_ui;
mod egui_intg;
mod gizmo;
mod image;
mod model;
mod vulkan;

use bevy::{app::AppExit, diagnostic::FrameTimeDiagnosticsPlugin, input::InputSystem, prelude::*};
use bevy_rapier3d::{physics::TimestepMode, prelude::*};
use config::{ApplyConfigSystem, Config, ConfigPlugin};
use contraption::{ContraptionPlugin, ContraptionUi, ContraptionUiParams};
use controller::ControllerPlugin;
use debug_ui::{DebugUi, DebugUiParams};
use egui_intg::{EguiCtx, EguiPlugin, InputLocked};
use gizmo::GizmoPlugin;
use image::{Image, ImagePlugin};
use model::{Model, ModelPlugin, RenderedBundle};
use vulkan::{CameraPos, LightPos, RenderPlugin, SkyboxImage};

const UP_VEC: Vec3 = Vec3::Y;
const BUILD_DATE: &str = env!("FG2_BUILD_DATE");
const GIT_REV: &str = env!("FG2_GIT_REV");

fn main() {
    App::new()
        .insert_resource(WindowDescriptor {
            title: format!("frizgame2 ({BUILD_DATE} - {GIT_REV})"),
            ..Default::default()
        })
        .add_plugins(DefaultPlugins)
        .add_plugin(FrameTimeDiagnosticsPlugin)
        .add_plugin(ConfigPlugin)
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::default())
        .insert_resource(RapierConfiguration {
            timestep_mode: TimestepMode::InterpolatedTimestep,
            ..Default::default()
        })
        .insert_resource(Grabbed(false))
        .insert_resource(CameraPos {
            pos: Vec3::new(0.0, 2.0, 5.0),
            direction: -Vec3::Z,
        })
        .add_system_to_stage(
            CoreStage::PreUpdate,
            update_grabbed.label(UpdateGrabbed).after(InputSystem),
        )
        .add_startup_stage(LoadAssetsStage::Load, SystemStage::parallel())
        .add_startup_system_to_stage(LoadAssetsStage::Load, load_assets)
        .add_startup_stage_after(
            LoadAssetsStage::Load,
            LoadAssetsStage::Inserted,
            SystemStage::parallel(),
        )
        .add_plugin(ControllerPlugin)
        .add_plugin(RenderPlugin)
        .add_plugin(ModelPlugin)
        .add_plugin(ImagePlugin)
        .add_plugin(EguiPlugin)
        .add_plugin(GizmoPlugin)
        .add_plugin(ContraptionPlugin)
        .add_startup_system_to_stage(LoadAssetsStage::Inserted, spawn_entities)
        .add_system(update_light)
        .add_system(ui.before(ApplyConfigSystem))
        .run();
}

struct LightDebugEntity(Entity);

struct PlayerEntity(Entity);

#[derive(Debug)]
pub struct AssetHandles {
    cube: Handle<Model>,
    light: Handle<Model>,
    skybox: Handle<Image>,
}

#[derive(Clone, Hash, Debug, PartialEq, Eq, StageLabel)]
enum LoadAssetsStage {
    Load,
    Inserted,
}

fn load_assets(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands.insert_resource(AssetHandles {
        cube: asset_server.load("cube.obj"),
        light: asset_server.load("light.obj"),
        skybox: asset_server.load("skybox.ktx2"),
    });
}

fn spawn_entities(mut commands: Commands, asset_handles: Option<Res<AssetHandles>>) {
    let asset_handles = asset_handles.as_ref().unwrap();

    let ground_size = Vec3::ONE * 10.0;
    let ground_pos = Vec3::new(0.0, -ground_size.y - 1.0, 0.0);
    commands
        .spawn()
        .insert_bundle(ColliderBundle {
            position: ground_pos.into(),
            shape: ColliderShape::cuboid(ground_size.x, ground_size.y, ground_size.z).into(),
            ..Default::default()
        })
        .insert_bundle(RenderedBundle {
            model: asset_handles.cube.clone(),
            transform: Transform::from_translation(ground_pos) * Transform::from_scale(ground_size),
            ..Default::default()
        });

    let light_debug = commands
        .spawn_bundle(RenderedBundle {
            model: asset_handles.light.clone(),
            ..Default::default()
        })
        .id();

    let mut player = commands.spawn();
    controller::init_player(&mut player);
    let player = player.id();

    commands.insert_resource(PlayerEntity(player));
    commands.insert_resource(LightDebugEntity(light_debug));
    commands.insert_resource(SkyboxImage(asset_handles.skybox.clone()));
}

fn update_light(
    mut light_pos: ResMut<LightPos>,
    time: Res<Time>,
    mut query: Query<&mut Transform>,
    light_debug_entity: ResMut<LightDebugEntity>,
) {
    let time = time.time_since_startup().as_secs_f64();
    let radius = 8.0;
    light_pos.0 = Vec3::new(time.cos() as f32 * radius, 10.0, time.sin() as f32 * radius);

    *query
        .get_component_mut::<Transform>(light_debug_entity.0)
        .unwrap() = Transform::from_translation(light_pos.0);
}

#[derive(Default)]
struct UiState {
    debug_ui: DebugUi,
    contraption_ui: ContraptionUi,
}

fn ui(
    mut state: Local<UiState>,
    egui: NonSendMut<EguiCtx>,
    mut exit_writer: EventWriter<AppExit>,
    mut config: ResMut<Config>,
    debug_params: DebugUiParams,
    contraption_params: ContraptionUiParams,
) {
    egui::Window::new("frizgame2")
        .anchor(egui::Align2::LEFT_TOP, [15.0, 15.0])
        .auto_sized()
        .collapsible(false)
        .show(&egui.ctx, |ui| {
            ui.set_min_width(200.0);
            ui.shrink_width_to_current();

            let quit_btn = ui.button(egui::RichText::new("Quit Game").color(egui::Color32::RED));
            if quit_btn.clicked() {
                exit_writer.send_default();
            }

            state.debug_ui.ui(ui, debug_params);
            config::ui(ui, &mut config);
            ui.separator();
            state.contraption_ui.ui(ui, contraption_params);
        });
}

#[derive(Debug, Hash, PartialEq, Eq, Clone, SystemLabel)]
pub struct UpdateGrabbed;

pub struct Grabbed(pub bool);

fn update_grabbed(
    input_locked: Res<InputLocked>,
    mut grabbed: ResMut<Grabbed>,
    keyboard: Res<Input<KeyCode>>,
    mut windows: ResMut<Windows>,
) {
    if !input_locked.0 && keyboard.just_pressed(KeyCode::KeyG) {
        grabbed.0 ^= true;

        let window = windows.get_primary_mut().unwrap();
        window.set_cursor_lock_mode(grabbed.0);
        window.set_cursor_visibility(!grabbed.0);
    }
}
