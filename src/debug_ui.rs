use bevy::{
    diagnostic::{Diagnostics, FrameTimeDiagnosticsPlugin},
    ecs::system::SystemParam,
    prelude::Res,
};
use egui::{plot, util::History};
use itertools::Itertools;
use std::{marker::PhantomData, time::Duration};

const FRAMETIME_MULTIPLIER: f64 = 1000.0;

#[derive(SystemParam)]
pub struct DebugUiParams<'w, 's> {
    diagnostics: Res<'w, Diagnostics>,
    #[system_param(ignore)]
    _lt: PhantomData<&'s usize>,
}

pub struct DebugUi {
    frametimes: History<f64>,
    paused_frametimes: Option<History<f64>>,
}

impl Default for DebugUi {
    fn default() -> Self {
        DebugUi {
            frametimes: History::new(0..usize::MAX, 3.0),
            paused_frametimes: None,
        }
    }
}

impl DebugUi {
    pub fn ui(&mut self, ui: &mut egui::Ui, params: DebugUiParams) {
        let fps = params
            .diagnostics
            .get(FrameTimeDiagnosticsPlugin::FPS)
            .unwrap();
        let frametime = params
            .diagnostics
            .get(FrameTimeDiagnosticsPlugin::FRAME_TIME)
            .unwrap();

        if let Some(average) = fps.average() {
            ui.label(format!("Average FPS: {:.2}", average));
        }

        if let Some(frametime) = frametime.value() {
            let value = frametime * FRAMETIME_MULTIPLIER;
            self.frametimes.add(ui.input().time, value);
        }

        if let Some(average) = frametime.average() {
            let frametime = Duration::from_secs_f64(average);
            ui.label(format!("Average Frametime: {:.2?}", frametime));
        }

        egui::CollapsingHeader::new("📊 Frametime Graph").show(ui, |ui| {
            let graphed_frametimes = self.paused_frametimes.as_ref().unwrap_or(&self.frametimes);
            let mut plot = plot::Plot::new(0)
                .allow_drag(false)
                .allow_zoom(false)
                .width(400.0)
                .height(100.0)
                .label_formatter(move |_, val| {
                    let frametime = Duration::from_secs_f64(val.y.max(0.0) / FRAMETIME_MULTIPLIER);
                    format!("Frametime: {:.2?}", frametime)
                });

            if !graphed_frametimes.is_empty() {
                let sum = graphed_frametimes.values().sum::<f64>();
                let average = sum / graphed_frametimes.len() as f64;
                let (min, max) = graphed_frametimes.values().minmax().into_option().unwrap();

                let lower_bound = (average - 3.0).min(min).max(0.0);
                let mut upper_bound = (average + 7.0).max(max);
                let highest_wanted = lower_bound + ((upper_bound - lower_bound) * 0.87);
                if max > highest_wanted {
                    upper_bound += max - highest_wanted;
                }

                plot = plot.include_y(lower_bound).include_y(upper_bound);
            }

            let values = graphed_frametimes
                .iter()
                .map(|(time, val)| plot::Value { x: time, y: val })
                .collect();

            plot.show(ui, |plot_ui| {
                if !plot_ui.plot_hovered() {
                    self.paused_frametimes = None;
                } else if self.paused_frametimes.is_none() {
                    self.paused_frametimes = Some(self.frametimes.clone());
                }

                plot_ui.line(plot::Line::new(plot::Values::from_values(values)));
            });
        });
    }
}
