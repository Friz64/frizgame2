use crate::{
    config::Config, egui_intg::InputLocked, vulkan::CameraPos, Grabbed, PlayerEntity, UP_VEC,
};
use bevy::{ecs::system::EntityCommands, input::mouse::MouseMotion, prelude::*};
use bevy_rapier3d::{
    physics::{PhysicsStages, PhysicsSystems},
    prelude::*,
};

const SCALE: f32 = 0.2;

pub struct ControllerPlugin;

impl Plugin for ControllerPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<CameraState>()
            .add_system(move_player)
            .add_system_to_stage(
                PhysicsStages::SyncTransforms,
                update_cam_pos.after(PhysicsSystems::SyncTransforms),
            );
    }
}

pub fn init_player(commands: &mut EntityCommands) {
    commands
        .insert_bundle(RigidBodyBundle {
            position: Vec3::new(0.0, 0.0, 5.0).into(),
            mass_properties: RigidBodyMassPropsFlags::ROTATION_LOCKED.into(),
            ..Default::default()
        })
        .insert_bundle(ColliderBundle {
            shape: ColliderShape::capsule(
                Point::from(-Vec3::Y * SCALE),
                Point::from(Vec3::Y * SCALE),
                SCALE,
            )
            .into(),
            material: ColliderMaterial {
                friction: 0.1,
                ..Default::default()
            }
            .into(),
            ..Default::default()
        })
        .insert(Transform::default())
        .insert(RigidBodyPositionSync::Interpolated { prev_pos: None });
}

#[derive(Default)]
struct CameraState {
    yaw: f32,
    pitch: f32,
    direction: Vec3,
}

#[allow(clippy::too_many_arguments)]
fn move_player(
    player: Option<Res<PlayerEntity>>,
    grabbed: Res<Grabbed>,
    input_locked: Res<InputLocked>,
    mut cam: ResMut<CameraState>,
    mut query: Query<(
        &RigidBodyMassPropsComponent,
        &mut RigidBodyVelocityComponent,
    )>,
    keyboard: Res<Input<KeyCode>>,
    mut mouse_motion: EventReader<MouseMotion>,
    config: Res<Config>,
    time: Res<Time>,
) {
    if grabbed.0 {
        let cursor_delta: Vec2 = mouse_motion
            .iter()
            .map(|mm| mm.delta)
            .fold(Vec2::ZERO, |acc, x| acc + x);

        let mouse_sensitivity = config.mouse_sensitivity * 0.05;
        cam.yaw += (cursor_delta.x * mouse_sensitivity) as f32;
        cam.pitch += (-cursor_delta.y * mouse_sensitivity) as f32;
        cam.pitch = cam.pitch.min(89.999).max(-89.999);
    }

    let direction = Vec3::new(
        cam.yaw.to_radians().sin() * cam.pitch.to_radians().cos(),
        cam.pitch.to_radians().sin(),
        -cam.yaw.to_radians().cos() * cam.pitch.to_radians().cos(),
    )
    .normalize();
    cam.direction = direction;

    if input_locked.0 {
        return;
    }

    if let Some(player) = player {
        let (mass_props, mut vel) = query.get_mut(player.0).unwrap();

        let mut movement = Vec3::ZERO;
        if keyboard.pressed(KeyCode::KeyW) {
            movement.z += 1.0;
        }

        if keyboard.pressed(KeyCode::KeyS) {
            movement.z -= 1.0;
        }

        if keyboard.pressed(KeyCode::KeyD) {
            movement.x += 1.0;
        }

        if keyboard.pressed(KeyCode::KeyA) {
            movement.x -= 1.0;
        }

        if keyboard.just_pressed(KeyCode::Space) {
            vel.apply_impulse(&mass_props.0, (Vec3::Y * 0.3).into());
        }

        let movement_len = movement.length();
        if movement_len > 1e-6 {
            movement /= movement_len;

            let mut force = Vec3::ZERO;
            force += movement.x * Vec3::cross(direction, UP_VEC).normalize();
            force += movement.z * direction;

            force.y = 0.0;
            force = force.normalize();
            force *= time.delta_seconds();

            vel.apply_impulse(&mass_props.0, force.into());
        }
    }
}

#[allow(clippy::too_many_arguments)]
fn update_cam_pos(
    player: Option<Res<PlayerEntity>>,
    query: Query<&Transform>,
    cam: Res<CameraState>,
    mut cam_pos: ResMut<CameraPos>,
) {
    if let Some(player) = player {
        let transform = query.get(player.0).unwrap();

        *cam_pos = CameraPos {
            pos: transform.translation + Vec3::Y * SCALE,
            direction: cam.direction,
        };
    }
}
