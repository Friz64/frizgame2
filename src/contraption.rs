mod scripting;

use self::scripting::{Scripted, ScriptingCtx, ScriptingPlugin};
use crate::{gizmo::Gizmo, model::Model, AssetHandles};
use bevy::{
    ecs::system::SystemParam,
    prelude::*,
    tasks::{IoTaskPool, Task},
    utils::HashSet,
};
use egui_gizmo::GizmoMode;
use erupt::SmallVec;
use futures_lite::future;
use itertools::Itertools;
use ron::ser::PrettyConfig;
use serde::{Deserialize, Serialize};
use std::{
    borrow::Cow,
    fs::{self, File},
};

const CONTRAPTIONS_DIR: &str = "./contraptions/";

pub struct ContraptionPlugin;

impl Plugin for ContraptionPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<IdCounter>().add_plugin(ScriptingPlugin);
    }
}

#[derive(Default)]
pub struct IdCounter(usize);

impl IdCounter {
    fn inc(&mut self) -> usize {
        self.0 += 1;
        self.0
    }
}

async fn error_msgbox(text: &str) {
    error!("{}", text);
    rfd::AsyncMessageDialog::new()
        .set_title("frizgame2")
        .set_level(rfd::MessageLevel::Error)
        .set_description(text)
        .show()
        .await;
}

#[derive(Debug, Serialize, Deserialize)]
struct ContraptionSave {
    name: String,
    spawn_pos: Vec3,
    script: String,
}

impl ContraptionSave {
    async fn save(contraption_save: ContraptionSave) {
        let _ = fs::create_dir(CONTRAPTIONS_DIR);
        if let Some(file) = rfd::AsyncFileDialog::new()
            .set_title("Save Contraption")
            .set_directory(CONTRAPTIONS_DIR)
            .set_file_name("contraption.ron")
            .save_file()
            .await
        {
            match ron::ser::to_string_pretty(&contraption_save, PrettyConfig::new()) {
                Ok(pretty) => {
                    if let Err(err) = fs::write(file.path(), pretty) {
                        error_msgbox(&format!("Failed to write contraption save: {err}")).await;
                    }
                }
                Err(err) => {
                    error_msgbox(&format!("Failed to serialize contraption save: {err}")).await;
                }
            };
        }
    }

    async fn load() -> Option<ContraptionSave> {
        let _ = fs::create_dir(CONTRAPTIONS_DIR);
        if let Some(file) = rfd::AsyncFileDialog::new()
            .set_title("Load Contraption")
            .set_directory(CONTRAPTIONS_DIR)
            .add_filter("frizgame2 Contraption RON File", &["ron"])
            .pick_file()
            .await
        {
            match File::open(file.path()) {
                Ok(file) => match ron::de::from_reader(file) {
                    Ok(save) => return Some(save),
                    Err(err) => {
                        error_msgbox(&format!("Failed to parse contraption save: {err}")).await;
                    }
                },
                Err(err) => error_msgbox(&format!("Failed to open contraption save: {err}")).await,
            }
        }

        None
    }
}

#[derive(Default, Component)]
pub struct Contraption {
    id: usize,
    name: String,
}

impl Contraption {
    fn id_name(&self) -> String {
        format!("Unnamed ({})", self.id)
    }

    fn name(&self) -> Cow<str> {
        if self.name.is_empty() {
            Cow::Owned(self.id_name())
        } else {
            Cow::Borrowed(&self.name)
        }
    }
}

#[derive(SystemParam)]
pub struct ContraptionUiParams<'w, 's> {
    commands: Commands<'w, 's>,
    id_counter: ResMut<'w, IdCounter>,
    scripting_ctx: NonSend<'w, ScriptingCtx>,
    contraptions: Query<
        'w,
        's,
        (
            Entity,
            &'static mut Contraption,
            &'static Transform,
            &'static mut Scripted,
        ),
    >,
    io_pool: Res<'w, IoTaskPool>,
    asset_handles: Res<'w, AssetHandles>,
}

#[derive(PartialEq, Clone)]
enum UiMode {
    Play,
    MoveSpawn(Entity),
    Design(Entity),
}

impl Default for UiMode {
    fn default() -> Self {
        UiMode::Play
    }
}

#[derive(Default)]
pub struct ContraptionUi {
    selected: Vec<Entity>,
    control_panels: HashSet<Entity>,
    pick_file_tasks: SmallVec<Task<Option<ContraptionSave>>>,
    prev_mode: UiMode,
    mode: UiMode,
}

fn new_contraption(
    commands: &mut Commands,
    id_counter: &mut IdCounter,
    name: String,
    spawn: Transform,
    script: Option<String>,
) -> Entity {
    let id = id_counter.inc();

    commands
        .spawn()
        .insert(Contraption { id, name })
        .insert_bundle(TransformBundle::from_transform(
            spawn.with_scale(Vec3::splat(0.2)),
        ))
        .insert(script.map(Scripted::from_script).unwrap_or_default())
        .id()
}

impl ContraptionUi {
    pub fn ui(&mut self, ui: &mut egui::Ui, mut params: ContraptionUiParams) {
        let selected = loop {
            if let Some(last) = self.selected.last() {
                match params.contraptions.get_mut(*last).ok() {
                    Some((entity, _contraption, _spawn, _scripted)) => {
                        break Some(entity);
                    }
                    None => {
                        self.selected.pop();
                    }
                }
            } else {
                break None;
            }
        };

        let mut selection_changed = false;
        let mut select = |entity| {
            self.selected.push(entity);
            selection_changed = true;
        };

        let mut remove = SmallVec::new();
        for (i, pick_file) in self.pick_file_tasks.iter_mut().enumerate() {
            if let Some(finished) = future::block_on(future::poll_once(&mut *pick_file)) {
                remove.push(i);
                if let Some(save) = finished {
                    select(new_contraption(
                        &mut params.commands,
                        &mut params.id_counter,
                        save.name,
                        Transform::from_translation(save.spawn_pos),
                        Some(save.script),
                    ));
                }
            }
        }

        for i in remove.into_iter().rev() {
            self.pick_file_tasks.swap_remove(i);
        }

        ui.horizontal(|ui| {
            if ui.button("New (default)").clicked() {
                select(new_contraption(
                    &mut params.commands,
                    &mut params.id_counter,
                    String::new(),
                    Transform::from_translation(Vec3::new(0.0, 5.0, 0.0)),
                    None,
                ));
            }

            if ui.button("Load from file").clicked() {
                let future = ContraptionSave::load();
                self.pick_file_tasks.push(params.io_pool.spawn(future));
            }
        });

        ui.separator();

        ui.add_enabled_ui(selected.is_some(), |ui| {
            ui.horizontal(|ui| {
                if ui.button("Duplicate").clicked() {
                    let (_entity, _contraption, spawn, existing_scripted) =
                        params.contraptions.get(selected.unwrap()).unwrap();
                    select(new_contraption(
                        &mut params.commands,
                        &mut params.id_counter,
                        String::new(),
                        *spawn,
                        Some(existing_scripted.script()),
                    ));
                }

                let despawn_btn =
                    ui.button(egui::RichText::new("Despawn").color(egui::Color32::RED));
                if despawn_btn.clicked() {
                    params.commands.entity(selected.unwrap()).despawn();
                }
            });
        });

        egui::ScrollArea::vertical()
            .max_height(300.0)
            .stick_to_bottom()
            .show(ui, |ui| {
                ui.set_width(ui.available_width());
                egui::Grid::new(ui.id().with("grid"))
                    .striped(true)
                    .show(ui, |ui| {
                        for (entity, contraption, _spawn, _scripted) in params
                            .contraptions
                            .iter()
                            .sorted_by_key(|(_, contraption, _spawn, _scripted)| contraption.id)
                        {
                            let radio_text = contraption.name().to_string();
                            let radio = ui.radio(selected == Some(entity), radio_text);

                            if radio.clicked() {
                                select(entity);
                            }

                            ui.with_layout(egui::Layout::top_down(egui::Align::Max), |ui| {
                                ui.set_min_width(100.0);
                                if ui.button("Control Panel").clicked() {
                                    self.control_panels.insert(entity);
                                }
                            });

                            ui.end_row();
                        }
                    });
            });

        let mut remove = HashSet::default();
        let mut window_layers = SmallVec::new();
        for &entity in &self.control_panels {
            let contraption_params = params.contraptions.get_mut(entity);
            if let Ok((entity, mut contraption, spawn, mut scripted)) = contraption_params {
                let mut keep = true;
                let contraption_window = egui::Window::new(contraption.name().to_string())
                    .id(egui::Id::new(entity))
                    .default_pos(egui::Pos2::new(15.0 + contraption.id as f32 * 50.0, 500.0))
                    .open(&mut keep)
                    .show(ui.ctx(), |ui| {
                        ui.horizontal(|ui| {
                            let hint_text = (contraption.name.is_empty())
                                .then(|| contraption.id_name())
                                .unwrap_or_default();
                            egui::TextEdit::singleline(&mut contraption.name)
                                .hint_text(hint_text)
                                .desired_width(100.0)
                                .show(ui);

                            if ui.button("Reset").clicked() {
                                scripted.reset();
                            }

                            if ui.button("Save to file").clicked() {
                                let future = ContraptionSave::save(ContraptionSave {
                                    name: contraption.name.clone(),
                                    spawn_pos: spawn.translation,
                                    script: scripted.script(),
                                });

                                params.io_pool.spawn(future).detach();
                            }

                            let mode = &mut self.mode;
                            ui.selectable_value(mode, UiMode::Play, "Play");
                            ui.selectable_value(mode, UiMode::MoveSpawn(entity), "Move Spawn");
                            ui.selectable_value(mode, UiMode::Design(entity), "Design");
                        });

                        ui.separator();
                        scripting::ui(ui, entity, &params.scripting_ctx, &mut scripted);
                    });

                if !keep {
                    remove.insert(entity);
                } else if let Some(window) = contraption_window {
                    window_layers.push((entity, window.response.layer_id));
                }
            }
        }

        for remove in remove {
            self.control_panels.remove(&remove);
        }

        let mut select_action = None;
        let mut move_to_top_action = None;
        let top_most_layer = ui.ctx().top_most_layer(egui::Order::Middle);
        for (entity, window_layer) in window_layers {
            let is_top_most = top_most_layer == Some(window_layer);
            let is_selected = self.selected.last() == Some(&entity);
            match (is_top_most, is_selected) {
                (true, false) => select_action = Some(entity),
                (false, true) => move_to_top_action = Some(window_layer),
                _ => (),
            }
        }

        match (selection_changed, select_action, move_to_top_action) {
            (false, Some(select), _) => self.selected.push(select),
            (true, _, Some(move_to_top)) => ui.ctx().move_to_top(move_to_top),
            _ => (),
        }

        if self.mode != self.prev_mode {
            if let UiMode::MoveSpawn(id) = &self.mode {
                let mut entity = params.commands.entity(*id);
                entity.insert(Gizmo(GizmoMode::Translate));
                entity.insert(params.asset_handles.cube.clone());
            }

            if let UiMode::MoveSpawn(id) = &self.prev_mode {
                let mut entity = params.commands.entity(*id);
                entity.remove::<Gizmo>();
                entity.remove::<Handle<Model>>();
            }
        }

        self.prev_mode = self.mode.clone();
    }
}
