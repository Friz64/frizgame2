use super::{ctx::VulkanCtx, utils::Barrier};
use erupt::{vk, SmallVec};
use std::slice;

fn inter_queue_buffer_barrier(
    barrier: vk::BufferMemoryBarrier2KHRBuilder,
) -> (
    vk::BufferMemoryBarrier2KHRBuilder<'static>,
    Option<vk::BufferMemoryBarrier2KHRBuilder<'static>>,
) {
    assert_ne!(barrier.src_queue_family_index, vk::QUEUE_FAMILY_IGNORED);
    assert_ne!(barrier.dst_queue_family_index, vk::QUEUE_FAMILY_IGNORED);
    assert!(barrier.p_next.is_null()); // no possibility for dangling pointers
    let barrier = barrier.build_dangling();

    if barrier.src_queue_family_index == barrier.dst_queue_family_index {
        let barrier = barrier
            .into_builder()
            .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED);
        (barrier, None)
    } else {
        let src_barrier = barrier
            .into_builder()
            .dst_stage_mask(vk::PipelineStageFlags2KHR::empty())
            .dst_access_mask(vk::AccessFlags2KHR::empty());
        let dst_barrier = barrier
            .into_builder()
            .src_stage_mask(vk::PipelineStageFlags2KHR::empty())
            .src_access_mask(vk::AccessFlags2KHR::empty());
        (src_barrier, Some(dst_barrier))
    }
}

fn inter_queue_image_barrier(
    barrier: vk::ImageMemoryBarrier2KHRBuilder,
) -> (
    vk::ImageMemoryBarrier2KHRBuilder<'static>,
    Option<vk::ImageMemoryBarrier2KHRBuilder<'static>>,
) {
    assert_ne!(barrier.src_queue_family_index, vk::QUEUE_FAMILY_IGNORED);
    assert_ne!(barrier.dst_queue_family_index, vk::QUEUE_FAMILY_IGNORED);
    assert!(barrier.p_next.is_null()); // no possibility for dangling pointers
    let barrier = barrier.build_dangling();

    if barrier.src_queue_family_index == barrier.dst_queue_family_index {
        let barrier = barrier
            .into_builder()
            .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED);
        (barrier, None)
    } else {
        let src_barrier = barrier
            .into_builder()
            .dst_stage_mask(vk::PipelineStageFlags2KHR::empty())
            .dst_access_mask(vk::AccessFlags2KHR::empty());
        let dst_barrier = barrier
            .into_builder()
            .src_stage_mask(vk::PipelineStageFlags2KHR::empty())
            .src_access_mask(vk::AccessFlags2KHR::empty());
        (src_barrier, Some(dst_barrier))
    }
}

pub struct TransferOp {
    pub cmd_buf: vk::CommandBuffer,
    fence: vk::Fence,
    dst_barriers: SmallVec<Barrier>,
}

impl TransferOp {
    pub unsafe fn new(ctx: &VulkanCtx) -> TransferOp {
        let cmd_buf = ctx
            .cmd_bufs
            .retrieve(ctx, ctx.transfer_queue.cmd_pool.get(ctx));
        let cmd_buf_info = vk::CommandBufferBeginInfoBuilder::new()
            .flags(vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT);
        ctx.device
            .begin_command_buffer(cmd_buf, &cmd_buf_info)
            .unwrap();

        let fence = ctx.fences.retrieve(ctx);

        TransferOp {
            cmd_buf,
            fence,
            dst_barriers: SmallVec::new(),
        }
    }

    pub unsafe fn buffer_ownership_transfer(
        &mut self,
        ctx: &VulkanCtx,
        barriers: &[vk::BufferMemoryBarrier2KHRBuilder],
    ) {
        let mut src_barriers = SmallVec::new();
        for barrier in barriers {
            let (src_barrier, dst_barrier) = inter_queue_buffer_barrier(*barrier);
            src_barriers.push(src_barrier);
            if let Some(dst_barrier) = dst_barrier {
                self.dst_barriers.push(Barrier::Buffer(dst_barrier));
            }
        }

        ctx.device.cmd_pipeline_barrier2_khr(
            self.cmd_buf,
            &vk::DependencyInfoKHRBuilder::new().buffer_memory_barriers(&src_barriers),
        );
    }

    pub unsafe fn image_ownership_transfer(
        &mut self,
        ctx: &VulkanCtx,
        barriers: &[vk::ImageMemoryBarrier2KHRBuilder],
    ) {
        let mut src_barriers = SmallVec::new();
        for barrier in barriers {
            let (src_barrier, dst_barrier) = inter_queue_image_barrier(*barrier);
            src_barriers.push(src_barrier);
            if let Some(dst_barrier) = dst_barrier {
                self.dst_barriers.push(Barrier::Image(dst_barrier));
            }
        }

        ctx.device.cmd_pipeline_barrier2_khr(
            self.cmd_buf,
            &vk::DependencyInfoKHRBuilder::new().image_memory_barriers(&src_barriers),
        );
    }

    pub unsafe fn finish(self, ctx: &VulkanCtx) -> SmallVec<Barrier> {
        ctx.device.end_command_buffer(self.cmd_buf).unwrap();

        let cmd_buf_submit_info =
            vk::CommandBufferSubmitInfoKHRBuilder::new().command_buffer(self.cmd_buf);
        let submit_info = vk::SubmitInfo2KHRBuilder::new()
            .command_buffer_infos(slice::from_ref(&cmd_buf_submit_info));

        ctx.device
            .queue_submit2_khr(ctx.transfer_queue.queue, &[submit_info], self.fence)
            .unwrap();

        ctx.device
            .wait_for_fences(slice::from_ref(&self.fence), true, u64::MAX)
            .unwrap();
        ctx.device
            .reset_fences(slice::from_ref(&self.fence))
            .unwrap();
        ctx.cmd_bufs
            .recycle(ctx.transfer_queue.cmd_pool.get(ctx), self.cmd_buf);
        ctx.fences.recycle(self.fence);

        self.dst_barriers
    }
}
