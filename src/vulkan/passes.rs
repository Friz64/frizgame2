mod egui_pass;
mod model_pass;
mod skybox_pass;

use self::{egui_pass::EguiPass, model_pass::ModelPass, skybox_pass::SkyboxPass};
use super::{
    ctx::{Lifetime, VulkanCtx},
    swapchain::{self, Frame},
    utils::AllocatedImage,
    RenderData, FRAMES_IN_FLIGHT,
};
use erupt::{vk, SmallVec};
use std::slice;
use vk_alloc::MemoryLocation;

const DEPTH_IMAGE_FORMAT: vk::Format = vk::Format::D32_SFLOAT;

struct InFlight {
    image_extent: vk::Extent2D,
    multisampled_image: AllocatedImage,
    depth_image: AllocatedImage,
}

impl InFlight {
    unsafe fn destroy(&self, ctx: &VulkanCtx) {
        self.multisampled_image.destroy(ctx);
        self.depth_image.destroy(ctx);
    }
}

pub struct Passes {
    in_flight: SmallVec<Option<InFlight>>,
    model: ModelPass,
    skybox: SkyboxPass,
    egui: EguiPass,
}

impl Passes {
    pub unsafe fn new(ctx: &VulkanCtx, swapchain_format_pair: vk::SurfaceFormatKHR) -> Passes {
        Passes {
            in_flight: (0..FRAMES_IN_FLIGHT).map(|_| None).collect(),
            model: ModelPass::new(ctx, swapchain_format_pair),
            skybox: SkyboxPass::new(ctx, swapchain_format_pair),
            egui: EguiPass::new(ctx, swapchain_format_pair),
        }
    }

    pub unsafe fn render(&mut self, ctx: &VulkanCtx, frame: &Frame, render_data: &RenderData) {
        ctx.device.cmd_pipeline_barrier2_khr(
            frame.cmd_buf(),
            &vk::DependencyInfoKHRBuilder::new().image_memory_barriers(&[
                vk::ImageMemoryBarrier2KHRBuilder::new()
                    .src_stage_mask(vk::PipelineStageFlags2KHR::COLOR_ATTACHMENT_OUTPUT_KHR)
                    .src_access_mask(vk::AccessFlags2KHR::empty())
                    .dst_stage_mask(vk::PipelineStageFlags2KHR::COLOR_ATTACHMENT_OUTPUT_KHR)
                    .dst_access_mask(vk::AccessFlags2KHR::COLOR_ATTACHMENT_WRITE_KHR)
                    .old_layout(vk::ImageLayout::UNDEFINED)
                    .new_layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                    .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                    .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                    .image(frame.image.handle)
                    .subresource_range(swapchain::SUBRESOURCE_RANGE),
            ]),
        );

        let in_flight = match &mut self.in_flight[frame.in_flight_idx()] {
            Some(in_flight) if in_flight.image_extent == frame.extent => in_flight,
            in_flight => {
                if let Some(in_flight) = in_flight {
                    in_flight.destroy(ctx);
                }

                let multisampled_image = {
                    let image_info = vk::ImageCreateInfoBuilder::new()
                        .format(frame.image_format.format)
                        .initial_layout(vk::ImageLayout::UNDEFINED)
                        .samples(ctx.highest_sample_count)
                        .tiling(vk::ImageTiling::OPTIMAL)
                        .usage(vk::ImageUsageFlags::COLOR_ATTACHMENT)
                        .sharing_mode(vk::SharingMode::EXCLUSIVE)
                        .image_type(vk::ImageType::_2D)
                        .mip_levels(1)
                        .array_layers(1)
                        .extent(vk::Extent3D {
                            width: frame.extent.width,
                            height: frame.extent.height,
                            depth: 1,
                        });

                    let subresource_range = vk::ImageSubresourceRange {
                        aspect_mask: vk::ImageAspectFlags::COLOR,
                        base_mip_level: 0,
                        level_count: 1,
                        base_array_layer: 0,
                        layer_count: 1,
                    };

                    let image_view_info = vk::ImageViewCreateInfoBuilder::new()
                        .format(image_info.format)
                        .view_type(vk::ImageViewType::_2D)
                        .subresource_range(subresource_range);

                    let image = AllocatedImage::new(
                        ctx,
                        &image_info,
                        MemoryLocation::GpuOnly,
                        Lifetime::FreedOnResize,
                        Some(image_view_info),
                    );

                    ctx.device.cmd_pipeline_barrier2_khr(
                        frame.cmd_buf(),
                        &vk::DependencyInfoKHRBuilder::new().image_memory_barriers(&[
                            vk::ImageMemoryBarrier2KHRBuilder::new()
                                .src_stage_mask(vk::PipelineStageFlags2KHR::empty())
                                .src_access_mask(vk::AccessFlags2KHR::empty())
                                .dst_stage_mask(
                                    vk::PipelineStageFlags2KHR::COLOR_ATTACHMENT_OUTPUT_KHR,
                                )
                                .dst_access_mask(vk::AccessFlags2KHR::COLOR_ATTACHMENT_WRITE_KHR)
                                .old_layout(vk::ImageLayout::UNDEFINED)
                                .new_layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                                .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                                .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                                .image(image.handle)
                                .subresource_range(subresource_range),
                        ]),
                    );

                    image
                };

                let depth_image = {
                    let image_info = vk::ImageCreateInfoBuilder::new()
                        .format(DEPTH_IMAGE_FORMAT)
                        .initial_layout(vk::ImageLayout::UNDEFINED)
                        .samples(ctx.highest_sample_count)
                        .tiling(vk::ImageTiling::OPTIMAL)
                        .usage(vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT)
                        .sharing_mode(vk::SharingMode::EXCLUSIVE)
                        .image_type(vk::ImageType::_2D)
                        .mip_levels(1)
                        .array_layers(1)
                        .extent(vk::Extent3D {
                            width: frame.extent.width,
                            height: frame.extent.height,
                            depth: 1,
                        });

                    let subresource_range = vk::ImageSubresourceRange {
                        aspect_mask: vk::ImageAspectFlags::DEPTH,
                        base_mip_level: 0,
                        level_count: 1,
                        base_array_layer: 0,
                        layer_count: 1,
                    };

                    let image_view_info = vk::ImageViewCreateInfoBuilder::new()
                        .format(image_info.format)
                        .view_type(vk::ImageViewType::_2D)
                        .subresource_range(subresource_range);

                    let image = AllocatedImage::new(
                        ctx,
                        &image_info,
                        MemoryLocation::GpuOnly,
                        Lifetime::FreedOnResize,
                        Some(image_view_info),
                    );

                    ctx.device.cmd_pipeline_barrier2_khr(
                        frame.cmd_buf(),
                        &vk::DependencyInfoKHRBuilder::new().image_memory_barriers(&[
                            vk::ImageMemoryBarrier2KHRBuilder::new()
                                .src_stage_mask(vk::PipelineStageFlags2KHR::empty())
                                .src_access_mask(vk::AccessFlags2KHR::empty())
                                .dst_stage_mask(
                                    vk::PipelineStageFlags2KHR::EARLY_FRAGMENT_TESTS_KHR
                                        | vk::PipelineStageFlags2KHR::LATE_FRAGMENT_TESTS_KHR,
                                )
                                .dst_access_mask(
                                    vk::AccessFlags2KHR::DEPTH_STENCIL_ATTACHMENT_WRITE_KHR,
                                )
                                .old_layout(vk::ImageLayout::UNDEFINED)
                                .new_layout(vk::ImageLayout::DEPTH_ATTACHMENT_OPTIMAL)
                                .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                                .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                                .image(image.handle)
                                .subresource_range(subresource_range),
                        ]),
                    );

                    image
                };

                *in_flight = Some(InFlight {
                    image_extent: frame.extent,
                    multisampled_image,
                    depth_image,
                });

                in_flight.as_mut().unwrap()
            }
        };

        let skybox_img_guard;
        let mut skybox_img = None;
        if let Some(img) = render_data.skybox.as_ref() {
            skybox_img_guard = ctx.images.read();
            skybox_img = skybox_img_guard.get(&img.0.id);
        }

        let color_attachment = vk::RenderingAttachmentInfoKHRBuilder::new()
            .image_view(in_flight.multisampled_image.view())
            .image_layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
            .load_op(if skybox_img.is_some() {
                vk::AttachmentLoadOp::DONT_CARE
            } else {
                vk::AttachmentLoadOp::CLEAR
            })
            .store_op(vk::AttachmentStoreOp::STORE)
            .clear_value(vk::ClearValue {
                color: vk::ClearColorValue {
                    float32: [0.039, 0.039, 0.039, 1.0],
                },
            })
            .resolve_mode(vk::ResolveModeFlagBits::AVERAGE)
            .resolve_image_view(frame.image.view)
            .resolve_image_layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL);

        let depth_attachment = vk::RenderingAttachmentInfoKHRBuilder::new()
            .image_view(in_flight.depth_image.view())
            .image_layout(vk::ImageLayout::DEPTH_ATTACHMENT_OPTIMAL)
            .load_op(vk::AttachmentLoadOp::CLEAR)
            .store_op(vk::AttachmentStoreOp::DONT_CARE)
            .clear_value(vk::ClearValue {
                depth_stencil: vk::ClearDepthStencilValue {
                    depth: 0.0,
                    stencil: 0,
                },
            });

        let rendering_info = vk::RenderingInfoKHRBuilder::new()
            .render_area(vk::Rect2D {
                offset: vk::Offset2D::default(),
                extent: frame.extent,
            })
            .layer_count(1)
            .color_attachments(slice::from_ref(&color_attachment))
            .depth_attachment(&depth_attachment);

        ctx.device
            .cmd_begin_rendering_khr(frame.cmd_buf(), &rendering_info);

        self.model.render(ctx, frame, render_data);
        if let Some(skybox_img) = skybox_img {
            self.skybox.render(ctx, frame, render_data, skybox_img);
        }

        ctx.device.cmd_end_rendering_khr(frame.cmd_buf());

        ctx.device.cmd_pipeline_barrier2_khr(
            frame.cmd_buf(),
            &vk::DependencyInfoKHRBuilder::new().image_memory_barriers(&[
                vk::ImageMemoryBarrier2KHRBuilder::new()
                    .src_stage_mask(vk::PipelineStageFlags2KHR::COLOR_ATTACHMENT_OUTPUT_KHR)
                    .src_access_mask(vk::AccessFlags2KHR::COLOR_ATTACHMENT_WRITE_KHR)
                    .dst_stage_mask(vk::PipelineStageFlags2KHR::COLOR_ATTACHMENT_OUTPUT_KHR)
                    .dst_access_mask(
                        vk::AccessFlags2KHR::COLOR_ATTACHMENT_READ_KHR
                            | vk::AccessFlags2KHR::COLOR_ATTACHMENT_WRITE_KHR,
                    )
                    .old_layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                    .new_layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                    .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                    .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                    .image(frame.image.handle)
                    .subresource_range(swapchain::SUBRESOURCE_RANGE),
            ]),
        );

        self.egui.prepare(ctx, frame, render_data);

        let color_attachment = vk::RenderingAttachmentInfoKHRBuilder::new()
            .image_view(frame.image.view)
            .image_layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
            .load_op(vk::AttachmentLoadOp::LOAD)
            .store_op(vk::AttachmentStoreOp::STORE);

        let rendering_info = vk::RenderingInfoKHRBuilder::new()
            .render_area(vk::Rect2D {
                offset: vk::Offset2D::default(),
                extent: frame.extent,
            })
            .layer_count(1)
            .color_attachments(slice::from_ref(&color_attachment));

        ctx.device
            .cmd_begin_rendering_khr(frame.cmd_buf(), &rendering_info);

        self.egui.render(ctx, frame, render_data);

        ctx.device.cmd_end_rendering_khr(frame.cmd_buf());

        ctx.device.cmd_pipeline_barrier2_khr(
            frame.cmd_buf(),
            &vk::DependencyInfoKHRBuilder::new().image_memory_barriers(&[
                vk::ImageMemoryBarrier2KHRBuilder::new()
                    .src_stage_mask(vk::PipelineStageFlags2KHR::COLOR_ATTACHMENT_OUTPUT_KHR)
                    .src_access_mask(vk::AccessFlags2KHR::COLOR_ATTACHMENT_WRITE_KHR)
                    .dst_stage_mask(vk::PipelineStageFlags2KHR::COLOR_ATTACHMENT_OUTPUT_KHR)
                    .dst_access_mask(vk::AccessFlags2KHR::empty())
                    .old_layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                    .new_layout(vk::ImageLayout::PRESENT_SRC_KHR)
                    .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                    .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                    .image(frame.image.handle)
                    .subresource_range(swapchain::SUBRESOURCE_RANGE),
            ]),
        );
    }

    pub unsafe fn destroy(&mut self, ctx: &VulkanCtx) {
        for in_flight in self.in_flight.iter().filter_map(|v| v.as_ref()) {
            in_flight.destroy(ctx);
        }

        self.model.destroy(ctx);
        self.skybox.destroy(ctx);
        self.egui.destroy(ctx);
    }
}
