use super::ctx::{BoundImage, Lifetime, VulkanCtx};
use crate::{model::ModelDesc, vulkan::transfer::TransferOp};
use erupt::{vk, SmallVec};
use std::borrow::Cow;
use vk_alloc::{Allocation, MemoryLocation};

pub unsafe fn shader_module(ctx: &VulkanCtx, bytes: &[u8]) -> vk::ShaderModule {
    let code = erupt::utils::decode_spv(bytes).unwrap();
    let module_info = vk::ShaderModuleCreateInfoBuilder::new().code(&code);
    ctx.device.create_shader_module(&module_info, None).unwrap()
}

pub enum Barrier {
    Buffer(vk::BufferMemoryBarrier2KHRBuilder<'static>),
    Image(vk::ImageMemoryBarrier2KHRBuilder<'static>),
}

impl Barrier {
    pub unsafe fn submit_barriers(
        ctx: &VulkanCtx,
        cmd_buf: vk::CommandBuffer,
        barriers: impl IntoIterator<Item = Barrier>,
    ) {
        let mut buffer_barriers = SmallVec::new();
        let mut image_barriers = SmallVec::new();
        for barrier in barriers.into_iter() {
            match barrier {
                Barrier::Buffer(barrier) => buffer_barriers.push(barrier),
                Barrier::Image(barrier) => image_barriers.push(barrier),
            }
        }

        ctx.device.cmd_pipeline_barrier2_khr(
            cmd_buf,
            &vk::DependencyInfoKHRBuilder::new()
                .buffer_memory_barriers(&buffer_barriers)
                .image_memory_barriers(&image_barriers),
        )
    }
}

unsafe impl Send for Barrier {}
unsafe impl Sync for Barrier {}

#[derive(Debug)]
pub struct AllocatedBuffer {
    size: vk::DeviceSize,
    pub handle: vk::Buffer,
    pub allocation: Allocation<Lifetime>,
}

impl AllocatedBuffer {
    pub unsafe fn new(
        ctx: &VulkanCtx,
        size: vk::DeviceSize,
        usage: vk::BufferUsageFlags,
        location: MemoryLocation,
        lifetime: Lifetime,
    ) -> AllocatedBuffer {
        let buffer_info = vk::BufferCreateInfoBuilder::new()
            .size(size)
            .usage(usage)
            .sharing_mode(vk::SharingMode::EXCLUSIVE);
        let buffer = ctx.device.create_buffer(&buffer_info, None).unwrap();

        let allocation = ctx
            .allocator
            .allocate_memory_for_buffer(&ctx.device, buffer, location, lifetime)
            .expect("Failed to allocate memory for buffer");

        ctx.device
            .bind_buffer_memory(buffer, allocation.device_memory(), allocation.offset())
            .unwrap();

        AllocatedBuffer {
            size,
            handle: buffer,
            allocation,
        }
    }

    pub unsafe fn _slice(&self) -> &[u8] {
        self.allocation.mapped_slice().unwrap().unwrap()
    }

    pub unsafe fn slice_mut(&mut self) -> &mut [u8] {
        self.allocation.mapped_slice_mut().unwrap().unwrap()
    }

    pub fn size(&self) -> vk::DeviceSize {
        self.size
    }

    pub unsafe fn destroy(&self, ctx: &VulkanCtx) {
        ctx.allocator
            .deallocate(&ctx.device, &self.allocation)
            .expect("Failed to deallocate buffer");
        ctx.device.destroy_buffer(self.handle, None);
    }
}

#[derive(Debug)]
pub struct AllocatedImage {
    pub handle: vk::Image,
    pub allocation: Allocation<Lifetime>,
    pub view: Option<vk::ImageView>,
}

impl AllocatedImage {
    pub unsafe fn new(
        ctx: &VulkanCtx,
        image_info: &vk::ImageCreateInfo,
        location: MemoryLocation,
        lifetime: Lifetime,
        view_info: Option<vk::ImageViewCreateInfoBuilder>,
    ) -> AllocatedImage {
        let is_optimal = image_info.tiling == vk::ImageTiling::OPTIMAL;
        let image = ctx.device.create_image(image_info, None).unwrap();
        let allocation = ctx
            .allocator
            .allocate_memory_for_image(&ctx.device, image, location, lifetime, is_optimal)
            .expect("Failed to allocate memory for image");

        ctx.device
            .bind_image_memory(image, allocation.device_memory(), allocation.offset())
            .unwrap();

        let view = view_info.map(|builder| {
            ctx.device
                .create_image_view(&builder.image(image), None)
                .unwrap()
        });

        AllocatedImage {
            handle: image,
            allocation,
            view,
        }
    }

    pub fn view(&self) -> vk::ImageView {
        self.view.unwrap()
    }

    pub unsafe fn _slice(&self) -> &[u8] {
        self.allocation.mapped_slice().unwrap().unwrap()
    }

    pub unsafe fn _slice_mut(&mut self) -> &mut [u8] {
        self.allocation.mapped_slice_mut().unwrap().unwrap()
    }

    pub unsafe fn destroy(&self, ctx: &VulkanCtx) {
        if let Some(view) = self.view {
            ctx.device.destroy_image_view(view, None);
        }

        ctx.allocator
            .deallocate(&ctx.device, &self.allocation)
            .expect("Failed to deallocate image");
        ctx.device.destroy_image(self.handle, None);
    }
}

pub struct VertexBuffers {
    pub vertex_buffer: AllocatedBuffer,
    pub index_buffer: AllocatedBuffer,
}

pub struct ImmutableModel {
    pub created_in_flight: Option<usize>,
    pub staging_buffers: Option<VertexBuffers>,
    pub gpu_buffers: VertexBuffers,
    pub index_count: u32,
}

impl ImmutableModel {
    pub unsafe fn new(
        ctx: &VulkanCtx,
        transfer: &mut TransferOp,
        model: &ModelDesc,
    ) -> ImmutableModel {
        let vertex_bytes = bytemuck::cast_slice::<_, u8>(&model.vertices).len();
        let index_bytes = bytemuck::cast_slice::<_, u8>(&model.indices).len();

        let mut staging_vertex_buffer = ctx.staging_buffers.retrieve(ctx, vertex_bytes as _);
        bytemuck::cast_slice_mut(&mut staging_vertex_buffer.slice_mut()[..vertex_bytes])
            .copy_from_slice(&model.vertices);

        let mut staging_index_buffer = ctx.staging_buffers.retrieve(ctx, index_bytes as _);
        bytemuck::cast_slice_mut(&mut staging_index_buffer.slice_mut()[..index_bytes])
            .copy_from_slice(&model.indices);

        ctx.device.cmd_pipeline_barrier2_khr(
            transfer.cmd_buf,
            &vk::DependencyInfoKHRBuilder::new().buffer_memory_barriers(&[
                vk::BufferMemoryBarrier2KHRBuilder::new()
                    .src_stage_mask(vk::PipelineStageFlags2KHR::HOST_KHR)
                    .src_access_mask(vk::AccessFlags2KHR::HOST_WRITE_KHR)
                    .dst_stage_mask(vk::PipelineStageFlags2KHR::TRANSFER_KHR)
                    .dst_access_mask(vk::AccessFlags2KHR::TRANSFER_READ_KHR)
                    .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                    .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                    .buffer(staging_vertex_buffer.handle)
                    .size(vertex_bytes as _),
                vk::BufferMemoryBarrier2KHRBuilder::new()
                    .src_stage_mask(vk::PipelineStageFlags2KHR::HOST_KHR)
                    .src_access_mask(vk::AccessFlags2KHR::HOST_WRITE_KHR)
                    .dst_stage_mask(vk::PipelineStageFlags2KHR::TRANSFER_KHR)
                    .dst_access_mask(vk::AccessFlags2KHR::TRANSFER_READ_KHR)
                    .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                    .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                    .buffer(staging_index_buffer.handle)
                    .size(index_bytes as _),
            ]),
        );

        let vertex_buffer = AllocatedBuffer::new(
            ctx,
            vertex_bytes as _,
            vk::BufferUsageFlags::TRANSFER_DST | vk::BufferUsageFlags::VERTEX_BUFFER,
            MemoryLocation::GpuOnly,
            Lifetime::EntireProgram,
        );
        ctx.device.cmd_copy_buffer(
            transfer.cmd_buf,
            staging_vertex_buffer.handle,
            vertex_buffer.handle,
            &[vk::BufferCopyBuilder::new().size(vertex_bytes as _)],
        );
        let vertex_barrier = vk::BufferMemoryBarrier2KHRBuilder::new()
            .src_stage_mask(vk::PipelineStageFlags2KHR::TRANSFER_KHR)
            .src_access_mask(vk::AccessFlags2KHR::TRANSFER_WRITE_KHR)
            .dst_stage_mask(vk::PipelineStageFlags2KHR::VERTEX_ATTRIBUTE_INPUT_KHR)
            .dst_access_mask(vk::AccessFlags2KHR::VERTEX_ATTRIBUTE_READ_KHR)
            .src_queue_family_index(ctx.transfer_queue.queue_family)
            .dst_queue_family_index(ctx.graphics_present_queue.queue_family)
            .buffer(vertex_buffer.handle)
            .size(vertex_bytes as _);

        let index_buffer = AllocatedBuffer::new(
            ctx,
            index_bytes as _,
            vk::BufferUsageFlags::TRANSFER_DST | vk::BufferUsageFlags::INDEX_BUFFER,
            MemoryLocation::GpuOnly,
            Lifetime::EntireProgram,
        );
        ctx.device.cmd_copy_buffer(
            transfer.cmd_buf,
            staging_index_buffer.handle,
            index_buffer.handle,
            &[vk::BufferCopyBuilder::new().size(index_bytes as _)],
        );
        let index_barrier = vk::BufferMemoryBarrier2KHRBuilder::new()
            .src_stage_mask(vk::PipelineStageFlags2KHR::TRANSFER_KHR)
            .src_access_mask(vk::AccessFlags2KHR::TRANSFER_WRITE_KHR)
            .dst_stage_mask(vk::PipelineStageFlags2KHR::INDEX_INPUT_KHR)
            .dst_access_mask(vk::AccessFlags2KHR::INDEX_READ_KHR)
            .src_queue_family_index(ctx.transfer_queue.queue_family)
            .dst_queue_family_index(ctx.graphics_present_queue.queue_family)
            .buffer(index_buffer.handle)
            .size(index_bytes as _);

        transfer.buffer_ownership_transfer(ctx, &[vertex_barrier, index_barrier]);

        ImmutableModel {
            created_in_flight: None,
            staging_buffers: Some(VertexBuffers {
                vertex_buffer: staging_vertex_buffer,
                index_buffer: staging_index_buffer,
            }),
            gpu_buffers: VertexBuffers {
                vertex_buffer,
                index_buffer,
            },
            index_count: model.indices.len() as _,
        }
    }

    pub unsafe fn recycle_staging(&mut self, ctx: &VulkanCtx) {
        if let Some(model_buffers) = self.staging_buffers.take() {
            ctx.staging_buffers.recycle(model_buffers.vertex_buffer);
            ctx.staging_buffers.recycle(model_buffers.index_buffer);
        }
    }

    pub unsafe fn destroy(&mut self, ctx: &VulkanCtx) {
        self.recycle_staging(ctx);
        self.gpu_buffers.vertex_buffer.destroy(ctx);
        self.gpu_buffers.index_buffer.destroy(ctx);
    }
}

pub struct Ktx2Image {
    staging_buffer: Option<AllocatedBuffer>,
    gpu_image: BoundImage,
}

impl Ktx2Image {
    pub unsafe fn new(
        ctx: &VulkanCtx,
        transfer: &mut TransferOp,
        header: ktx2::Header,
        levels: SmallVec<Cow<[u8]>>,
    ) -> Ktx2Image {
        let format = header.format.expect("KTX2 must specify a Vulkan format");
        let vk_format = vk::Format(format.0.get() as _);
        let width = header.pixel_width;
        let height = header.pixel_height;
        let depth = header.pixel_depth;
        let array_layers = header.layer_count;
        let cubemap_faces = header.face_count;

        let bytes: usize = levels.iter().map(|level| level.len()).sum();
        let layer_count = array_layers.max(1);
        let extent = vk::Extent3D {
            width: width.max(1),
            height: height.max(1),
            depth: depth.max(1),
        };

        let has_depth = depth != 0;
        let has_height = height != 0;
        let image_type = if has_depth {
            vk::ImageType::_3D
        } else if has_height {
            vk::ImageType::_2D
        } else {
            vk::ImageType::_1D
        };

        let view_type = match (image_type, cubemap_faces > 1, array_layers > 1) {
            (vk::ImageType::_1D, false, false) => vk::ImageViewType::_1D,
            (vk::ImageType::_2D, false, false) => vk::ImageViewType::_2D,
            (vk::ImageType::_3D, false, false) => vk::ImageViewType::_3D,
            (vk::ImageType::_1D, false, true) => vk::ImageViewType::_1D_ARRAY,
            (vk::ImageType::_2D, false, true) => vk::ImageViewType::_2D_ARRAY,
            (_, true, false) => vk::ImageViewType::CUBE,
            (_, true, true) => vk::ImageViewType::CUBE_ARRAY,
            criteria => panic!("Failed to determine image view type for ({criteria:?})"),
        };

        let mut image_flags = vk::ImageCreateFlags::empty();
        if cubemap_faces > 1 {
            image_flags |= vk::ImageCreateFlags::CUBE_COMPATIBLE;
        }

        let image_info = vk::ImageCreateInfoBuilder::new()
            .image_type(image_type)
            .format(vk_format)
            .mip_levels(levels.len() as _)
            .samples(vk::SampleCountFlagBits::_1)
            .tiling(vk::ImageTiling::OPTIMAL)
            .sharing_mode(vk::SharingMode::EXCLUSIVE)
            .initial_layout(vk::ImageLayout::UNDEFINED)
            .extent(extent)
            .usage(vk::ImageUsageFlags::TRANSFER_DST | vk::ImageUsageFlags::SAMPLED)
            .array_layers(layer_count * cubemap_faces)
            .flags(image_flags);

        let format_props = ctx.instance.get_physical_device_image_format_properties(
            ctx.physical_device,
            image_info.format,
            image_info.image_type,
            image_info.tiling,
            image_info.usage,
            image_info.flags,
        );

        match format_props.result() {
            Ok(format_props) => {
                assert!(format_props.max_extent.width >= image_info.extent.width);
                assert!(format_props.max_extent.height >= image_info.extent.height);
                assert!(format_props.max_extent.depth >= image_info.extent.depth);
                assert!(format_props.max_mip_levels >= image_info.mip_levels);
                assert!(format_props.max_resource_size >= bytes as vk::DeviceSize);
            }
            Err(err) => panic!("can't use texture ({err:?})"),
        };

        let subresource_range = vk::ImageSubresourceRange {
            aspect_mask: vk::ImageAspectFlags::COLOR,
            base_mip_level: 0,
            level_count: image_info.mip_levels,
            base_array_layer: 0,
            layer_count: image_info.array_layers,
        };

        let image_view_info = vk::ImageViewCreateInfoBuilder::new()
            .format(image_info.format)
            .view_type(view_type)
            .subresource_range(subresource_range);

        let gpu_image = AllocatedImage::new(
            ctx,
            &image_info,
            MemoryLocation::GpuOnly,
            Lifetime::EntireProgram,
            Some(image_view_info),
        );

        let mut staging_buffer = ctx.staging_buffers.retrieve(ctx, bytes as _);
        let mut buffer_copy_regions = SmallVec::new();
        let mut level_offset = 0;
        for (level_idx, level) in levels.iter().enumerate() {
            let level_end = level_offset + level.len();
            bytemuck::cast_slice_mut(&mut staging_buffer.slice_mut()[level_offset..level_end])
                .copy_from_slice(level);

            let layer_size = level.len() as u64 / layer_count as u64;
            let face_size = layer_size / cubemap_faces as u64;
            for layer in 0..layer_count {
                let layer_offset = level_offset as u64 + layer_size * layer as u64;
                for face in 0..cubemap_faces {
                    let face_offset = layer_offset + face_size * face as u64;
                    let copy_region = vk::BufferImageCopyBuilder::new()
                        .image_subresource(vk::ImageSubresourceLayers {
                            aspect_mask: vk::ImageAspectFlags::COLOR,
                            mip_level: level_idx as _,
                            base_array_layer: layer * cubemap_faces + face,
                            layer_count: 1,
                        })
                        .image_extent(vk::Extent3D {
                            width: extent.width >> level_idx,
                            height: extent.height >> (level_idx * (has_height as usize)),
                            depth: extent.depth >> (level_idx * (has_depth as usize)),
                        })
                        .buffer_offset(face_offset);
                    buffer_copy_regions.push(copy_region);
                }
            }

            level_offset = level_end;
        }

        ctx.device.cmd_pipeline_barrier2_khr(
            transfer.cmd_buf,
            &vk::DependencyInfoKHRBuilder::new()
                .buffer_memory_barriers(&[vk::BufferMemoryBarrier2KHRBuilder::new()
                    .src_stage_mask(vk::PipelineStageFlags2KHR::HOST_KHR)
                    .src_access_mask(vk::AccessFlags2KHR::HOST_WRITE_KHR)
                    .dst_stage_mask(vk::PipelineStageFlags2KHR::TRANSFER_KHR)
                    .dst_access_mask(vk::AccessFlags2KHR::TRANSFER_READ_KHR)
                    .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                    .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                    .buffer(staging_buffer.handle)
                    .size(bytes as _)])
                .image_memory_barriers(&[vk::ImageMemoryBarrier2KHRBuilder::new()
                    .src_stage_mask(vk::PipelineStageFlags2KHR::HOST_KHR)
                    .src_access_mask(vk::AccessFlags2KHR::HOST_WRITE_KHR)
                    .dst_stage_mask(vk::PipelineStageFlags2KHR::TRANSFER_KHR)
                    .dst_access_mask(vk::AccessFlags2KHR::TRANSFER_WRITE_KHR)
                    .old_layout(vk::ImageLayout::UNDEFINED)
                    .new_layout(vk::ImageLayout::TRANSFER_DST_OPTIMAL)
                    .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                    .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                    .image(gpu_image.handle)
                    .subresource_range(subresource_range)]),
        );

        ctx.device.cmd_copy_buffer_to_image(
            transfer.cmd_buf,
            staging_buffer.handle,
            gpu_image.handle,
            vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            &buffer_copy_regions,
        );

        transfer.image_ownership_transfer(
            ctx,
            &[vk::ImageMemoryBarrier2KHRBuilder::new()
                .src_stage_mask(vk::PipelineStageFlags2KHR::TRANSFER_KHR)
                .src_access_mask(vk::AccessFlags2KHR::TRANSFER_WRITE_KHR)
                .dst_stage_mask(vk::PipelineStageFlags2KHR::FRAGMENT_SHADER_KHR)
                .dst_access_mask(vk::AccessFlags2KHR::SHADER_SAMPLED_READ_KHR)
                .src_queue_family_index(ctx.transfer_queue.queue_family)
                .dst_queue_family_index(ctx.graphics_present_queue.queue_family)
                .old_layout(vk::ImageLayout::TRANSFER_DST_OPTIMAL)
                .new_layout(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL)
                .image(gpu_image.handle)
                .subresource_range(subresource_range)],
        );

        let gpu_image =
            ctx.image_set
                .retrieve(ctx, gpu_image, vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL);

        Ktx2Image {
            staging_buffer: Some(staging_buffer),
            gpu_image,
        }
    }

    pub unsafe fn recycle_staging(&mut self, ctx: &VulkanCtx) {
        if let Some(staging_buffer) = self.staging_buffer.take() {
            ctx.staging_buffers.recycle(staging_buffer);
        }
    }

    pub fn gpu_image(&self) -> BoundImage {
        self.gpu_image
    }

    pub unsafe fn destroy(&mut self, ctx: &VulkanCtx) {
        self.recycle_staging(ctx);
        ctx.image_set.release(self.gpu_image);
    }
}
