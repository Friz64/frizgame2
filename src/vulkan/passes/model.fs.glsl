#version 450

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inLightPos;
layout(location = 3) in vec3 inViewPos;

layout(location = 0) out vec4 outColor;

void main() {
    vec3 base_color = inNormal * 0.5 + vec3(0.5);
    vec3 light_color = vec3(0.3);

    vec3 ambient = 0.05 * base_color;

    vec3 light_dir = normalize(inLightPos - inPos);
    vec3 normal = normalize(inNormal);
    float diff = dot(light_dir, normal) * 0.5 + 0.5;
    vec3 diffuse = diff * base_color;

    vec3 view_dir = normalize(inViewPos - inPos);
    vec3 halfway_dir = normalize(light_dir + view_dir);
    float spec = pow(max(dot(normal, halfway_dir), 0.0), 256.0);
    vec3 specular = light_color * spec;

    outColor = vec4(ambient + diffuse + specular, 1.0);
}
