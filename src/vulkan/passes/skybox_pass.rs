use crate::vulkan::{
    ctx::{BoundImage, VulkanCtx},
    passes::DEPTH_IMAGE_FORMAT,
    swapchain::Frame,
    utils::{self, Ktx2Image},
    RenderData,
};
use bevy::prelude::*;
use erupt::{cstr, vk, ExtendableFrom};
use std::{ffi::CStr, mem, ptr, slice};

#[repr(C)]
struct VertexPushConstants {
    view_projection: Mat4,
}

#[repr(C)]
struct FragmentPushConstants {
    image: BoundImage,
}

pub struct SkyboxPass {
    pipeline: vk::Pipeline,
    pipeline_layout: vk::PipelineLayout,
}

impl SkyboxPass {
    pub unsafe fn new(ctx: &VulkanCtx, swapchain_format_pair: vk::SurfaceFormatKHR) -> SkyboxPass {
        let push_constants = [
            vk::PushConstantRangeBuilder::new()
                .stage_flags(vk::ShaderStageFlags::VERTEX)
                .offset(0)
                .size(mem::size_of::<VertexPushConstants>() as _),
            vk::PushConstantRangeBuilder::new()
                .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                .offset(mem::size_of::<VertexPushConstants>() as _)
                .size(mem::size_of::<FragmentPushConstants>() as _),
        ];

        let image_set_layout = ctx.image_set.set_layout();
        let pipeline_layout_info = vk::PipelineLayoutCreateInfoBuilder::new()
            .set_layouts(slice::from_ref(&image_set_layout))
            .push_constant_ranges(&push_constants);
        let pipeline_layout = ctx
            .device
            .create_pipeline_layout(&pipeline_layout_info, None)
            .unwrap();

        let vs_module = utils::shader_module(ctx, include_bytes!("skybox.vs.spv"));
        let fs_module = utils::shader_module(ctx, include_bytes!("skybox.fs.spv"));
        let main_cstr = CStr::from_ptr(cstr!("main"));
        let shader_stages = [
            vk::PipelineShaderStageCreateInfoBuilder::new()
                .stage(vk::ShaderStageFlagBits::VERTEX)
                .module(vs_module)
                .name(main_cstr),
            vk::PipelineShaderStageCreateInfoBuilder::new()
                .stage(vk::ShaderStageFlagBits::FRAGMENT)
                .module(fs_module)
                .name(main_cstr),
        ];

        let input_assembly_info = vk::PipelineInputAssemblyStateCreateInfoBuilder::new()
            .topology(vk::PrimitiveTopology::TRIANGLE_LIST);

        let viewport_info = vk::PipelineViewportStateCreateInfoBuilder::new()
            .viewport_count(1)
            .scissor_count(1);

        let dynamic_states = [vk::DynamicState::VIEWPORT, vk::DynamicState::SCISSOR];
        let dynamic_state_info =
            vk::PipelineDynamicStateCreateInfoBuilder::new().dynamic_states(&dynamic_states);

        let rasterization_info = vk::PipelineRasterizationStateCreateInfoBuilder::new()
            .depth_clamp_enable(false)
            .rasterizer_discard_enable(false)
            .polygon_mode(vk::PolygonMode::FILL)
            .line_width(1.0)
            .cull_mode(vk::CullModeFlags::BACK)
            .front_face(vk::FrontFace::COUNTER_CLOCKWISE)
            .depth_bias_enable(false);

        let depth_stencil_info = vk::PipelineDepthStencilStateCreateInfoBuilder::new()
            .depth_test_enable(true)
            .depth_write_enable(true)
            .depth_compare_op(vk::CompareOp::GREATER_OR_EQUAL)
            .stencil_test_enable(false);

        let color_blend_attachments = [vk::PipelineColorBlendAttachmentStateBuilder::new()
            .color_write_mask(
                vk::ColorComponentFlags::R
                    | vk::ColorComponentFlags::G
                    | vk::ColorComponentFlags::B
                    | vk::ColorComponentFlags::A,
            )
            .blend_enable(false)];
        let color_blend_state = vk::PipelineColorBlendStateCreateInfoBuilder::new()
            .attachments(&color_blend_attachments);

        let vertex_input_state = vk::PipelineVertexInputStateCreateInfoBuilder::new();

        let multisample_info = vk::PipelineMultisampleStateCreateInfoBuilder::new()
            .rasterization_samples(ctx.highest_sample_count);

        let color_attachment_formats = [swapchain_format_pair.format];
        let mut pipeline_rendering_info = vk::PipelineRenderingCreateInfoKHRBuilder::new()
            .color_attachment_formats(&color_attachment_formats)
            .depth_attachment_format(DEPTH_IMAGE_FORMAT);

        let pipeline_info = vk::GraphicsPipelineCreateInfoBuilder::new()
            .stages(&shader_stages)
            .vertex_input_state(&vertex_input_state)
            .input_assembly_state(&input_assembly_info)
            .viewport_state(&viewport_info)
            .rasterization_state(&rasterization_info)
            .multisample_state(&multisample_info)
            .depth_stencil_state(&depth_stencil_info)
            .color_blend_state(&color_blend_state)
            .dynamic_state(&dynamic_state_info)
            .layout(pipeline_layout)
            .extend_from(&mut pipeline_rendering_info);

        let pipeline_infos = slice::from_ref(&pipeline_info);
        let pipeline = ctx
            .device
            .create_graphics_pipelines(vk::PipelineCache::null(), pipeline_infos, None)
            .expect("Failed to create pipeline")[0];

        ctx.device.destroy_shader_module(vs_module, None);
        ctx.device.destroy_shader_module(fs_module, None);

        SkyboxPass {
            pipeline,
            pipeline_layout,
        }
    }

    pub unsafe fn render(
        &mut self,
        ctx: &VulkanCtx,
        frame: &Frame,
        render_data: &RenderData,
        skybox_image: &Ktx2Image,
    ) {
        ctx.device.cmd_bind_pipeline(
            frame.cmd_buf(),
            vk::PipelineBindPoint::GRAPHICS,
            self.pipeline,
        );

        ctx.device.cmd_bind_descriptor_sets(
            frame.cmd_buf(),
            vk::PipelineBindPoint::GRAPHICS,
            self.pipeline_layout,
            0,
            &[ctx.image_set.descriptor_set()],
            &[],
        );

        ctx.device.cmd_set_viewport(
            frame.cmd_buf(),
            0,
            &[vk::ViewportBuilder::new()
                .width(frame.extent.width as _)
                .height(frame.extent.height as _)
                .min_depth(0.0)
                .max_depth(1.0)],
        );
        ctx.device.cmd_set_scissor(
            frame.cmd_buf(),
            0,
            &[vk::Rect2DBuilder::new().extent(frame.extent)],
        );

        let (mut view, projection) = render_data.cam_pos.view_projection_matrices(
            render_data.fov,
            frame.extent.width as _,
            frame.extent.height as _,
            true,
        );

        // always keep cube at camera position
        *view.col_mut(3) = Vec4::new(0.0, 0.0, 0.0, 1.0);

        let vertex_push_constants = VertexPushConstants {
            view_projection: projection * view,
        };
        ctx.device.cmd_push_constants(
            frame.cmd_buf(),
            self.pipeline_layout,
            vk::ShaderStageFlags::VERTEX,
            0,
            mem::size_of::<VertexPushConstants>() as _,
            ptr::addr_of!(vertex_push_constants) as _,
        );

        let fragment_push_constants = FragmentPushConstants {
            image: skybox_image.gpu_image(),
        };
        ctx.device.cmd_push_constants(
            frame.cmd_buf(),
            self.pipeline_layout,
            vk::ShaderStageFlags::FRAGMENT,
            mem::size_of::<VertexPushConstants>() as _,
            mem::size_of::<FragmentPushConstants>() as _,
            ptr::addr_of!(fragment_push_constants) as _,
        );

        ctx.device.cmd_draw(frame.cmd_buf(), 36, 1, 0, 0);
    }

    pub unsafe fn destroy(&mut self, ctx: &VulkanCtx) {
        ctx.device.destroy_pipeline(self.pipeline, None);
        ctx.device
            .destroy_pipeline_layout(self.pipeline_layout, None);
    }
}
