use crate::vulkan::{
    ctx::{BoundImage, Lifetime, VulkanCtx},
    swapchain::Frame,
    utils::{self, AllocatedBuffer, AllocatedImage},
    RenderData, FRAMES_IN_FLIGHT,
};
use bevy::{
    prelude::*,
    utils::{HashMap, HashSet},
};
use egui::epaint;
use erupt::{cstr, vk, ExtendableFrom, SmallVec};
use itertools::Itertools;
use memoffset::offset_of;
use std::{ffi::CStr, mem, ptr, slice};
use vk_alloc::MemoryLocation;

#[repr(C)]
struct VertexPushConstants {
    screen_size: Vec2,
}

#[repr(C)]
struct FragmentPushConstants {
    image: BoundImage,
}

struct EguiImage {
    extent: vk::Extent3D,
    first_used_in_flight: usize,
    destroy_in_flight: usize,
    image: BoundImage,
    staging_buffer: Option<AllocatedBuffer>,
}

impl EguiImage {
    unsafe fn new(
        ctx: &VulkanCtx,
        frame: &Frame,
        old: Option<&EguiImage>,
        delta: &epaint::ImageDelta,
    ) -> EguiImage {
        let data: &[u8] = match &delta.image {
            egui::ImageData::Color(img) => bytemuck::cast_slice(&img.pixels),
            egui::ImageData::Alpha(img) => bytemuck::cast_slice(&img.pixels),
        };

        let update_extent = vk::Extent3D {
            width: delta.image.width() as u32,
            height: delta.image.height() as u32,
            depth: 1,
        };

        let mut staging_buffer = ctx
            .staging_buffers
            .retrieve(ctx, data.len() as vk::DeviceSize);
        staging_buffer.slice_mut()[..data.len()].copy_from_slice(data);

        let full_extent = if delta.is_whole() {
            update_extent
        } else {
            old.unwrap().extent
        };

        let subresource_range = vk::ImageSubresourceRange {
            aspect_mask: vk::ImageAspectFlags::COLOR,
            base_mip_level: 0,
            level_count: 1,
            base_array_layer: 0,
            layer_count: 1,
        };

        let image_info = vk::ImageCreateInfoBuilder::new()
            .format(vk::Format::R8_UNORM)
            .initial_layout(vk::ImageLayout::UNDEFINED)
            .samples(vk::SampleCountFlagBits::_1)
            .tiling(vk::ImageTiling::OPTIMAL)
            .usage(
                vk::ImageUsageFlags::SAMPLED
                    | vk::ImageUsageFlags::TRANSFER_SRC
                    | vk::ImageUsageFlags::TRANSFER_DST,
            )
            .sharing_mode(vk::SharingMode::EXCLUSIVE)
            .image_type(vk::ImageType::_2D)
            .mip_levels(1)
            .array_layers(1)
            .extent(full_extent);
        let image_view_info = vk::ImageViewCreateInfoBuilder::new()
            .format(image_info.format)
            .view_type(vk::ImageViewType::_2D)
            .subresource_range(subresource_range);

        let image = AllocatedImage::new(
            ctx,
            &image_info,
            MemoryLocation::GpuOnly,
            Lifetime::FreedRarely,
            Some(image_view_info),
        );

        ctx.device.cmd_pipeline_barrier2_khr(
            frame.cmd_buf(),
            &vk::DependencyInfoKHRBuilder::new().image_memory_barriers(&[
                vk::ImageMemoryBarrier2KHRBuilder::new()
                    .src_stage_mask(vk::PipelineStageFlags2KHR::HOST_KHR)
                    .src_access_mask(vk::AccessFlags2KHR::HOST_WRITE_KHR)
                    .dst_stage_mask(vk::PipelineStageFlags2KHR::TRANSFER_KHR)
                    .dst_access_mask(vk::AccessFlags2KHR::TRANSFER_WRITE_KHR)
                    .old_layout(vk::ImageLayout::UNDEFINED)
                    .new_layout(vk::ImageLayout::TRANSFER_DST_OPTIMAL)
                    .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                    .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                    .image(image.handle)
                    .subresource_range(subresource_range),
            ]),
        );

        let image_subresource = vk::ImageSubresourceLayers {
            aspect_mask: vk::ImageAspectFlags::COLOR,
            mip_level: 0,
            base_array_layer: 0,
            layer_count: 1,
        };

        let mut update_offset = vk::Offset3D { x: 0, y: 0, z: 0 };
        if let Some(sub_region_pos) = delta.pos {
            let old_image = old.unwrap().image;
            let old_handle = ctx.image_set.access(old_image, |image| image.handle);

            ctx.device.cmd_pipeline_barrier2_khr(
                frame.cmd_buf(),
                &vk::DependencyInfoKHRBuilder::new().image_memory_barriers(&[
                    vk::ImageMemoryBarrier2KHRBuilder::new()
                        .src_stage_mask(vk::PipelineStageFlags2KHR::FRAGMENT_SHADER_KHR)
                        .src_access_mask(vk::AccessFlags2KHR::SHADER_SAMPLED_READ_KHR)
                        .dst_stage_mask(vk::PipelineStageFlags2KHR::TRANSFER_KHR)
                        .dst_access_mask(vk::AccessFlags2KHR::TRANSFER_READ_KHR)
                        .old_layout(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL)
                        .new_layout(vk::ImageLayout::TRANSFER_SRC_OPTIMAL)
                        .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                        .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                        .image(old_handle)
                        .subresource_range(subresource_range),
                ]),
            );

            ctx.device.cmd_copy_image(
                frame.cmd_buf(),
                old_handle,
                vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
                image.handle,
                vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                &[vk::ImageCopyBuilder::new()
                    .extent(full_extent)
                    .src_subresource(image_subresource)
                    .dst_subresource(image_subresource)],
            );

            ctx.device.cmd_pipeline_barrier2_khr(
                frame.cmd_buf(),
                &vk::DependencyInfoKHRBuilder::new().image_memory_barriers(&[
                    vk::ImageMemoryBarrier2KHRBuilder::new()
                        .src_stage_mask(vk::PipelineStageFlags2KHR::TRANSFER_KHR)
                        .src_access_mask(vk::AccessFlags2KHR::TRANSFER_WRITE_KHR)
                        .dst_stage_mask(vk::PipelineStageFlags2KHR::TRANSFER_KHR)
                        .dst_access_mask(vk::AccessFlags2KHR::TRANSFER_WRITE_KHR)
                        .old_layout(vk::ImageLayout::TRANSFER_DST_OPTIMAL)
                        .new_layout(vk::ImageLayout::TRANSFER_DST_OPTIMAL)
                        .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                        .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                        .image(image.handle)
                        .subresource_range(subresource_range),
                ]),
            );

            update_offset = vk::Offset3D {
                x: sub_region_pos[0] as i32,
                y: sub_region_pos[1] as i32,
                z: 0,
            };
        }

        ctx.device.cmd_copy_buffer_to_image(
            frame.cmd_buf(),
            staging_buffer.handle,
            image.handle,
            vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            &[vk::BufferImageCopyBuilder::new()
                .image_offset(update_offset)
                .image_extent(update_extent)
                .image_subresource(image_subresource)],
        );

        ctx.device.cmd_pipeline_barrier2_khr(
            frame.cmd_buf(),
            &vk::DependencyInfoKHRBuilder::new().image_memory_barriers(&[
                vk::ImageMemoryBarrier2KHRBuilder::new()
                    .src_stage_mask(vk::PipelineStageFlags2KHR::TRANSFER_KHR)
                    .src_access_mask(vk::AccessFlags2KHR::TRANSFER_WRITE_KHR)
                    .dst_stage_mask(vk::PipelineStageFlags2KHR::FRAGMENT_SHADER_KHR)
                    .dst_access_mask(vk::AccessFlags2KHR::SHADER_SAMPLED_READ_KHR)
                    .old_layout(vk::ImageLayout::TRANSFER_DST_OPTIMAL)
                    .new_layout(vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL)
                    .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                    .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                    .image(image.handle)
                    .subresource_range(subresource_range),
            ]),
        );

        let image = ctx
            .image_set
            .retrieve(ctx, image, vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL);

        EguiImage {
            extent: full_extent,
            first_used_in_flight: usize::MAX,
            destroy_in_flight: usize::MAX,
            image,
            staging_buffer: Some(staging_buffer),
        }
    }

    unsafe fn recycle_staging(&mut self, ctx: &VulkanCtx) {
        if let Some(staging_buffer) = self.staging_buffer.take() {
            ctx.staging_buffers.recycle(staging_buffer);
        }
    }

    unsafe fn destroy(&mut self, ctx: &VulkanCtx) {
        self.recycle_staging(ctx);
        ctx.image_set.release(self.image);
    }
}

struct EguiTexture {
    last_used_in_flight: usize,
    current: Option<EguiImage>,
    to_be_destroyed: SmallVec<EguiImage>,
}

impl Default for EguiTexture {
    fn default() -> Self {
        EguiTexture {
            last_used_in_flight: usize::MAX,
            current: None,
            to_be_destroyed: SmallVec::new(),
        }
    }
}

impl EguiTexture {
    unsafe fn recycle_staging(&mut self, ctx: &VulkanCtx, frame: &Frame) {
        let current_image = self.current.as_mut().unwrap();
        if current_image.first_used_in_flight == frame.in_flight_idx() {
            current_image.recycle_staging(ctx);
        }
    }

    unsafe fn update(&mut self, ctx: &VulkanCtx, frame: &Frame, delta: &epaint::ImageDelta) {
        self.last_used_in_flight = frame.in_flight_idx();

        let old_image = self.current.take();
        self.current = Some(EguiImage::new(ctx, frame, old_image.as_ref(), delta));
        if let Some(old_image) = old_image {
            self.to_be_destroyed.push(old_image);
        }
    }

    unsafe fn image(&mut self, ctx: &VulkanCtx, frame: &Frame) -> BoundImage {
        self.last_used_in_flight = frame.in_flight_idx();

        let destroy: SmallVec<_> = self
            .to_be_destroyed
            .iter()
            .positions(|image| image.destroy_in_flight == frame.in_flight_idx())
            .collect();
        for i in destroy.into_iter().rev() {
            self.to_be_destroyed.swap_remove(i).destroy(ctx);
        }

        let current_image = self.current.as_mut().unwrap();
        for image in self.to_be_destroyed.iter_mut() {
            if image.destroy_in_flight == usize::MAX {
                image.destroy_in_flight = frame.in_flight_idx();
            }
        }

        current_image.image
    }

    unsafe fn destroy(&mut self, ctx: &VulkanCtx) {
        for image in self.to_be_destroyed.iter_mut().chain(self.current.as_mut()) {
            image.destroy(ctx);
        }
    }
}

struct InFlight {
    vertex_buffer: AllocatedBuffer,
    index_buffer: AllocatedBuffer,
}

pub struct EguiPass {
    pipeline: vk::Pipeline,
    pipeline_layout: vk::PipelineLayout,
    in_flight: SmallVec<InFlight>,
    textures: HashMap<egui::TextureId, EguiTexture>,
    tex_free_requested: HashSet<egui::TextureId>,
}

impl EguiPass {
    pub unsafe fn new(ctx: &VulkanCtx, swapchain_format_pair: vk::SurfaceFormatKHR) -> EguiPass {
        let push_constants = [
            vk::PushConstantRangeBuilder::new()
                .stage_flags(vk::ShaderStageFlags::VERTEX)
                .offset(0)
                .size(mem::size_of::<VertexPushConstants>() as _),
            vk::PushConstantRangeBuilder::new()
                .stage_flags(vk::ShaderStageFlags::FRAGMENT)
                .offset(mem::size_of::<VertexPushConstants>() as _)
                .size(mem::size_of::<FragmentPushConstants>() as _),
        ];

        let image_set_layout = ctx.image_set.set_layout();
        let pipeline_layout_info = vk::PipelineLayoutCreateInfoBuilder::new()
            .set_layouts(slice::from_ref(&image_set_layout))
            .push_constant_ranges(&push_constants);
        let pipeline_layout = ctx
            .device
            .create_pipeline_layout(&pipeline_layout_info, None)
            .unwrap();

        let bindings = [vk::VertexInputBindingDescriptionBuilder::new()
            .binding(0)
            .input_rate(vk::VertexInputRate::VERTEX)
            .stride(mem::size_of::<epaint::Vertex>() as _)];

        let attributes = [
            // pos
            vk::VertexInputAttributeDescriptionBuilder::new()
                .binding(0)
                .offset(offset_of!(epaint::Vertex, pos) as _)
                .location(0)
                .format(vk::Format::R32G32_SFLOAT),
            // uv
            vk::VertexInputAttributeDescriptionBuilder::new()
                .binding(0)
                .offset(offset_of!(epaint::Vertex, uv) as _)
                .location(1)
                .format(vk::Format::R32G32_SFLOAT),
            // color
            vk::VertexInputAttributeDescriptionBuilder::new()
                .binding(0)
                .offset(offset_of!(epaint::Vertex, color) as _)
                .location(2)
                .format(vk::Format::R8G8B8A8_UNORM),
        ];

        let vs_module = utils::shader_module(ctx, include_bytes!("egui.vs.spv"));
        let fs_module = utils::shader_module(ctx, include_bytes!("egui.fs.spv"));
        let main_cstr = CStr::from_ptr(cstr!("main"));
        let shader_stages = [
            vk::PipelineShaderStageCreateInfoBuilder::new()
                .stage(vk::ShaderStageFlagBits::VERTEX)
                .module(vs_module)
                .name(main_cstr),
            vk::PipelineShaderStageCreateInfoBuilder::new()
                .stage(vk::ShaderStageFlagBits::FRAGMENT)
                .module(fs_module)
                .name(main_cstr),
        ];

        let input_assembly_info = vk::PipelineInputAssemblyStateCreateInfoBuilder::new()
            .topology(vk::PrimitiveTopology::TRIANGLE_LIST);

        let viewport_info = vk::PipelineViewportStateCreateInfoBuilder::new()
            .viewport_count(1)
            .scissor_count(1);

        let rasterization_info = vk::PipelineRasterizationStateCreateInfoBuilder::new()
            .depth_clamp_enable(false)
            .rasterizer_discard_enable(false)
            .polygon_mode(vk::PolygonMode::FILL)
            .cull_mode(vk::CullModeFlags::NONE)
            .front_face(vk::FrontFace::COUNTER_CLOCKWISE)
            .depth_bias_enable(false)
            .line_width(1.0);

        let stencil_op = vk::StencilOpStateBuilder::new()
            .fail_op(vk::StencilOp::KEEP)
            .pass_op(vk::StencilOp::KEEP)
            .compare_op(vk::CompareOp::ALWAYS)
            .build();
        let depth_stencil_info = vk::PipelineDepthStencilStateCreateInfoBuilder::new()
            .depth_test_enable(false)
            .depth_write_enable(false)
            .depth_compare_op(vk::CompareOp::ALWAYS)
            .depth_bounds_test_enable(false)
            .stencil_test_enable(false)
            .front(stencil_op)
            .back(stencil_op);

        let color_blend_attachments = [vk::PipelineColorBlendAttachmentStateBuilder::new()
            .color_write_mask(
                vk::ColorComponentFlags::R
                    | vk::ColorComponentFlags::G
                    | vk::ColorComponentFlags::B
                    | vk::ColorComponentFlags::A,
            )
            .blend_enable(true)
            .src_color_blend_factor(vk::BlendFactor::ONE)
            .dst_color_blend_factor(vk::BlendFactor::ONE_MINUS_SRC_ALPHA)];
        let color_blend_state = vk::PipelineColorBlendStateCreateInfoBuilder::new()
            .attachments(&color_blend_attachments);

        let dynamic_states = [vk::DynamicState::VIEWPORT, vk::DynamicState::SCISSOR];
        let dynamic_state_info =
            vk::PipelineDynamicStateCreateInfoBuilder::new().dynamic_states(&dynamic_states);

        let vertex_input_state = vk::PipelineVertexInputStateCreateInfoBuilder::new()
            .vertex_attribute_descriptions(&attributes)
            .vertex_binding_descriptions(&bindings);

        let multisample_info = vk::PipelineMultisampleStateCreateInfoBuilder::new()
            .rasterization_samples(vk::SampleCountFlagBits::_1);

        let color_attachment_formats = [swapchain_format_pair.format];
        let mut pipeline_rendering_info = vk::PipelineRenderingCreateInfoKHRBuilder::new()
            .color_attachment_formats(&color_attachment_formats);

        let pipeline_info = vk::GraphicsPipelineCreateInfoBuilder::new()
            .stages(&shader_stages)
            .vertex_input_state(&vertex_input_state)
            .input_assembly_state(&input_assembly_info)
            .viewport_state(&viewport_info)
            .rasterization_state(&rasterization_info)
            .multisample_state(&multisample_info)
            .depth_stencil_state(&depth_stencil_info)
            .color_blend_state(&color_blend_state)
            .dynamic_state(&dynamic_state_info)
            .layout(pipeline_layout)
            .extend_from(&mut pipeline_rendering_info);

        let pipeline_infos = slice::from_ref(&pipeline_info);
        let pipeline = ctx
            .device
            .create_graphics_pipelines(vk::PipelineCache::null(), pipeline_infos, None)
            .expect("Failed to create pipeline")[0];

        ctx.device.destroy_shader_module(vs_module, None);
        ctx.device.destroy_shader_module(fs_module, None);

        let in_flight = (0..FRAMES_IN_FLIGHT)
            .map(|_| {
                let vertex_buffer = AllocatedBuffer::new(
                    ctx,
                    1024 * 1024,
                    vk::BufferUsageFlags::VERTEX_BUFFER,
                    MemoryLocation::CpuToGpu,
                    Lifetime::EntireProgram,
                );

                let index_buffer = AllocatedBuffer::new(
                    ctx,
                    1024 * 1024,
                    vk::BufferUsageFlags::INDEX_BUFFER,
                    MemoryLocation::CpuToGpu,
                    Lifetime::EntireProgram,
                );

                InFlight {
                    vertex_buffer,
                    index_buffer,
                }
            })
            .collect();

        EguiPass {
            pipeline,
            pipeline_layout,
            in_flight,
            textures: HashMap::default(),
            tex_free_requested: HashSet::default(),
        }
    }

    pub unsafe fn prepare(&mut self, ctx: &VulkanCtx, frame: &Frame, render_data: &RenderData) {
        for texture in self.textures.values_mut() {
            texture.recycle_staging(ctx, frame);
        }

        let textures_delta = &render_data.egui.textures_delta;

        self.tex_free_requested.extend(textures_delta.free.iter());
        let mut tex_to_free: HashSet<_> = self.tex_free_requested.clone();

        for (tex_id, delta) in &textures_delta.set {
            tex_to_free.remove(tex_id);

            let texture = self.textures.entry(*tex_id).or_default();
            texture.update(ctx, frame, delta);
        }

        for tex_id in tex_to_free.into_iter() {
            if self.textures[&tex_id].last_used_in_flight == frame.in_flight_idx() {
                self.textures.remove(&tex_id).unwrap().destroy(ctx);
                self.tex_free_requested.remove(&tex_id);
            }
        }
    }

    pub unsafe fn render(&mut self, ctx: &VulkanCtx, frame: &Frame, render_data: &RenderData) {
        let in_flight = &mut self.in_flight[frame.in_flight_idx()];
        let scale_factor = render_data.scale_factor as f32;

        ctx.device.cmd_bind_pipeline(
            frame.cmd_buf(),
            vk::PipelineBindPoint::GRAPHICS,
            self.pipeline,
        );

        ctx.device.cmd_bind_vertex_buffers(
            frame.cmd_buf(),
            0,
            &[in_flight.vertex_buffer.handle],
            &[0],
        );

        ctx.device.cmd_bind_index_buffer(
            frame.cmd_buf(),
            in_flight.index_buffer.handle,
            0,
            vk::IndexType::UINT32,
        );

        ctx.device.cmd_bind_descriptor_sets(
            frame.cmd_buf(),
            vk::PipelineBindPoint::GRAPHICS,
            self.pipeline_layout,
            0,
            &[ctx.image_set.descriptor_set()],
            &[],
        );

        ctx.device.cmd_set_viewport(
            frame.cmd_buf(),
            0,
            &[vk::ViewportBuilder::new()
                .x(0.0)
                .y(0.0)
                .width(frame.extent.width as _)
                .height(frame.extent.height as _)
                .min_depth(0.0)
                .max_depth(1.0)],
        );

        let vertex_push_constants = VertexPushConstants {
            screen_size: Vec2::new(frame.extent.width as _, frame.extent.height as _)
                / scale_factor,
        };

        ctx.device.cmd_push_constants(
            frame.cmd_buf(),
            self.pipeline_layout,
            vk::ShaderStageFlags::VERTEX,
            0,
            mem::size_of::<VertexPushConstants>() as _,
            ptr::addr_of!(vertex_push_constants) as _,
        );

        let mut texture_bindings = HashMap::default();
        let mut vertex_base = 0;
        let mut vertex_bytes_base = 0;
        let mut index_base = 0;
        let mut index_bytes_base = 0;
        for clipped_primitive in &render_data.egui.primitives {
            let clip_rect = clipped_primitive.clip_rect;
            let mesh = match &clipped_primitive.primitive {
                epaint::Primitive::Mesh(mesh) => mesh,
                epaint::Primitive::Callback(_) => continue,
            };

            let fragment_push_constants = FragmentPushConstants {
                image: *texture_bindings.entry(mesh.texture_id).or_insert_with(|| {
                    let egui_texture = &mut self.textures.get_mut(&mesh.texture_id).unwrap();
                    egui_texture.image(ctx, frame)
                }),
            };

            ctx.device.cmd_push_constants(
                frame.cmd_buf(),
                self.pipeline_layout,
                vk::ShaderStageFlags::FRAGMENT,
                mem::size_of::<VertexPushConstants>() as _,
                mem::size_of::<FragmentPushConstants>() as _,
                ptr::addr_of!(fragment_push_constants) as _,
            );

            let vertex_bytes = mesh.vertices.len() * mem::size_of::<epaint::Vertex>();
            let vertex_range = vertex_bytes_base..vertex_bytes_base + vertex_bytes;
            bytemuck::cast_slice_mut(&mut in_flight.vertex_buffer.slice_mut()[vertex_range])
                .copy_from_slice(&mesh.vertices);
            vertex_bytes_base += vertex_bytes;

            let index_bytes = mesh.indices.len() * mem::size_of::<u32>();
            let index_range = index_bytes_base..index_bytes_base + index_bytes;
            bytemuck::cast_slice_mut(&mut in_flight.index_buffer.slice_mut()[index_range])
                .copy_from_slice(&mesh.indices);
            index_bytes_base += index_bytes;

            let frame_extent = egui::pos2(frame.extent.width as _, frame.extent.height as _);

            let scaled_min = egui::pos2(
                clip_rect.min.x * scale_factor,
                clip_rect.min.y * scale_factor,
            );
            let min = scaled_min.clamp(egui::Pos2::ZERO, frame_extent).round();

            let scaled_max = egui::pos2(
                clip_rect.max.x * scale_factor,
                clip_rect.max.y * scale_factor,
            );
            let max = scaled_max.clamp(egui::Pos2::ZERO, frame_extent).round();

            ctx.device.cmd_set_scissor(
                frame.cmd_buf(),
                0,
                &[vk::Rect2DBuilder::new()
                    .offset(vk::Offset2D {
                        x: min.x as _,
                        y: min.y as _,
                    })
                    .extent(vk::Extent2D {
                        width: (max.x - min.x) as _,
                        height: (max.y - min.y) as _,
                    })],
            );

            ctx.device.cmd_draw_indexed(
                frame.cmd_buf(),
                mesh.indices.len() as _,
                1,
                index_base as _,
                vertex_base as _,
                0,
            );

            vertex_base += mesh.vertices.len();
            index_base += mesh.indices.len();
        }
    }

    pub unsafe fn destroy(&mut self, ctx: &VulkanCtx) {
        for texture in self.textures.values_mut() {
            texture.destroy(ctx);
        }

        for in_flight in &self.in_flight {
            in_flight.vertex_buffer.destroy(ctx);
            in_flight.index_buffer.destroy(ctx);
        }

        ctx.device.destroy_pipeline(self.pipeline, None);
        ctx.device
            .destroy_pipeline_layout(self.pipeline_layout, None);
    }
}
