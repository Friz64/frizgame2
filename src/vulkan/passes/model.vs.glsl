#version 450
#extension GL_EXT_debug_printf : enable

layout(location = 0) in vec3 inPos;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in mat4 inModel;

layout(location = 0) out vec3 outPos;
layout(location = 1) out vec3 outNormal;
layout(location = 2) out vec3 outLightPos;
layout(location = 3) out vec3 outViewPos;

layout(push_constant) uniform PushConstants {
    mat4 pcViewProjection;
    vec3 pcLightPos;
    vec3 pcViewPos;
};

void main() {
    gl_Position = pcViewProjection * inModel * vec4(inPos, 1.0);
    outPos = inPos;
    outNormal = inNormal;

    mat4 inverse_model = inverse(inModel);
    outLightPos = (inverse_model * vec4(pcLightPos, 1.0)).xyz;
    outViewPos = (inverse_model * vec4(pcViewPos, 1.0)).xyz;
}
