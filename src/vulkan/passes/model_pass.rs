use crate::{
    model::Vertex,
    vulkan::{
        ctx::{Lifetime, VulkanCtx},
        passes::DEPTH_IMAGE_FORMAT,
        swapchain::Frame,
        utils::{self, AllocatedBuffer},
        RenderData, FRAMES_IN_FLIGHT,
    },
};
use bevy::{math::Vec3A, prelude::*, utils::HashMap};
use erupt::{cstr, vk, ExtendableFrom, SmallVec};
use memoffset::offset_of;
use std::{ffi::CStr, mem, ptr, slice};
use vk_alloc::MemoryLocation;

const MAX_OBJECTS: usize = 16384;

#[repr(C)]
struct VertexPushConstants {
    view_projection: Mat4,
    light_pos: Vec3A,
    view_pos: Vec3A,
}

struct InFlight {
    instance_buffer: AllocatedBuffer,
}

pub struct ModelPass {
    pipeline: vk::Pipeline,
    pipeline_layout: vk::PipelineLayout,
    in_flight: SmallVec<InFlight>,
}

impl ModelPass {
    pub unsafe fn new(ctx: &VulkanCtx, swapchain_format_pair: vk::SurfaceFormatKHR) -> ModelPass {
        let push_constants = [vk::PushConstantRangeBuilder::new()
            .stage_flags(vk::ShaderStageFlags::VERTEX)
            .offset(0)
            .size(mem::size_of::<VertexPushConstants>() as _)];

        let pipeline_layout_info =
            vk::PipelineLayoutCreateInfoBuilder::new().push_constant_ranges(&push_constants);
        let pipeline_layout = ctx
            .device
            .create_pipeline_layout(&pipeline_layout_info, None)
            .unwrap();

        let bindings = [
            vk::VertexInputBindingDescriptionBuilder::new()
                .binding(0)
                .input_rate(vk::VertexInputRate::VERTEX)
                .stride(mem::size_of::<Vertex>() as _),
            vk::VertexInputBindingDescriptionBuilder::new()
                .binding(1)
                .input_rate(vk::VertexInputRate::INSTANCE)
                .stride(mem::size_of::<Mat4>() as _),
        ];

        let mut attributes = vec![
            // pos
            vk::VertexInputAttributeDescriptionBuilder::new()
                .binding(0)
                .offset(offset_of!(Vertex, pos) as _)
                .location(0)
                .format(vk::Format::R32G32B32_SFLOAT),
            // normal
            vk::VertexInputAttributeDescriptionBuilder::new()
                .binding(0)
                .offset(offset_of!(Vertex, normal) as _)
                .location(1)
                .format(vk::Format::R32G32B32_SFLOAT),
        ];

        for i in 0..4 {
            attributes.push(
                vk::VertexInputAttributeDescriptionBuilder::new()
                    .binding(1)
                    .offset((mem::size_of::<Vec4>() * i) as _)
                    .location((2 + i) as _)
                    .format(vk::Format::R32G32B32A32_SFLOAT),
            );
        }

        let vs_module = utils::shader_module(ctx, include_bytes!("model.vs.spv"));
        let fs_module = utils::shader_module(ctx, include_bytes!("model.fs.spv"));
        let main_cstr = CStr::from_ptr(cstr!("main"));
        let shader_stages = [
            vk::PipelineShaderStageCreateInfoBuilder::new()
                .stage(vk::ShaderStageFlagBits::VERTEX)
                .module(vs_module)
                .name(main_cstr),
            vk::PipelineShaderStageCreateInfoBuilder::new()
                .stage(vk::ShaderStageFlagBits::FRAGMENT)
                .module(fs_module)
                .name(main_cstr),
        ];

        let input_assembly_info = vk::PipelineInputAssemblyStateCreateInfoBuilder::new()
            .topology(vk::PrimitiveTopology::TRIANGLE_LIST);

        let viewport_info = vk::PipelineViewportStateCreateInfoBuilder::new()
            .viewport_count(1)
            .scissor_count(1);

        let dynamic_states = [vk::DynamicState::VIEWPORT, vk::DynamicState::SCISSOR];
        let dynamic_state_info =
            vk::PipelineDynamicStateCreateInfoBuilder::new().dynamic_states(&dynamic_states);

        let rasterization_info = vk::PipelineRasterizationStateCreateInfoBuilder::new()
            .depth_clamp_enable(false)
            .rasterizer_discard_enable(false)
            .polygon_mode(vk::PolygonMode::FILL)
            .line_width(1.0)
            .cull_mode(vk::CullModeFlags::BACK)
            .front_face(vk::FrontFace::COUNTER_CLOCKWISE)
            .depth_bias_enable(false);

        let depth_stencil_info = vk::PipelineDepthStencilStateCreateInfoBuilder::new()
            .depth_test_enable(true)
            .depth_write_enable(true)
            .depth_compare_op(vk::CompareOp::GREATER)
            .stencil_test_enable(false);

        let color_blend_attachments = [vk::PipelineColorBlendAttachmentStateBuilder::new()
            .color_write_mask(
                vk::ColorComponentFlags::R
                    | vk::ColorComponentFlags::G
                    | vk::ColorComponentFlags::B
                    | vk::ColorComponentFlags::A,
            )
            .blend_enable(false)];
        let color_blend_state = vk::PipelineColorBlendStateCreateInfoBuilder::new()
            .attachments(&color_blend_attachments);

        let vertex_input_state = vk::PipelineVertexInputStateCreateInfoBuilder::new()
            .vertex_attribute_descriptions(&attributes)
            .vertex_binding_descriptions(&bindings);

        let multisample_info = vk::PipelineMultisampleStateCreateInfoBuilder::new()
            .rasterization_samples(ctx.highest_sample_count);

        let color_attachment_formats = [swapchain_format_pair.format];
        let mut pipeline_rendering_info = vk::PipelineRenderingCreateInfoKHRBuilder::new()
            .color_attachment_formats(&color_attachment_formats)
            .depth_attachment_format(DEPTH_IMAGE_FORMAT);

        let pipeline_info = vk::GraphicsPipelineCreateInfoBuilder::new()
            .stages(&shader_stages)
            .vertex_input_state(&vertex_input_state)
            .input_assembly_state(&input_assembly_info)
            .viewport_state(&viewport_info)
            .rasterization_state(&rasterization_info)
            .multisample_state(&multisample_info)
            .depth_stencil_state(&depth_stencil_info)
            .color_blend_state(&color_blend_state)
            .dynamic_state(&dynamic_state_info)
            .layout(pipeline_layout)
            .extend_from(&mut pipeline_rendering_info);

        let pipeline_infos = slice::from_ref(&pipeline_info);
        let pipeline = ctx
            .device
            .create_graphics_pipelines(vk::PipelineCache::null(), pipeline_infos, None)
            .expect("Failed to create pipeline")[0];

        ctx.device.destroy_shader_module(vs_module, None);
        ctx.device.destroy_shader_module(fs_module, None);

        ModelPass {
            pipeline,
            pipeline_layout,
            in_flight: (0..FRAMES_IN_FLIGHT)
                .map(|_| {
                    let instance_buffer = AllocatedBuffer::new(
                        ctx,
                        (MAX_OBJECTS * mem::size_of::<Mat4>()) as _,
                        vk::BufferUsageFlags::VERTEX_BUFFER,
                        MemoryLocation::CpuToGpu,
                        Lifetime::EntireProgram,
                    );

                    InFlight { instance_buffer }
                })
                .collect(),
        }
    }

    pub unsafe fn render(&mut self, ctx: &VulkanCtx, frame: &Frame, render_data: &RenderData) {
        let allocated_models = ctx.models.read();
        let in_flight = &mut self.in_flight[frame.in_flight_idx()];

        let mut model_matrices: HashMap<_, Vec<_>> = Default::default();
        for (handle, transform) in &render_data.models {
            if allocated_models.contains_key(&handle.id) {
                let model = transform.compute_matrix();
                model_matrices.entry(handle.id).or_default().push(model);
            }
        }

        ctx.device.cmd_bind_pipeline(
            frame.cmd_buf(),
            vk::PipelineBindPoint::GRAPHICS,
            self.pipeline,
        );

        ctx.device.cmd_set_viewport(
            frame.cmd_buf(),
            0,
            &[vk::ViewportBuilder::new()
                .width(frame.extent.width as _)
                .height(frame.extent.height as _)
                .min_depth(0.0)
                .max_depth(1.0)],
        );
        ctx.device.cmd_set_scissor(
            frame.cmd_buf(),
            0,
            &[vk::Rect2DBuilder::new().extent(frame.extent)],
        );

        let (view, projection) = render_data.cam_pos.view_projection_matrices(
            render_data.fov,
            frame.extent.width as _,
            frame.extent.height as _,
            true,
        );
        let vertex_push_constants = VertexPushConstants {
            view_projection: projection * view,
            light_pos: render_data.light_pos.into(),
            view_pos: render_data.cam_pos.pos.into(),
        };
        ctx.device.cmd_push_constants(
            frame.cmd_buf(),
            self.pipeline_layout,
            vk::ShaderStageFlags::VERTEX,
            0,
            mem::size_of::<VertexPushConstants>() as _,
            ptr::addr_of!(vertex_push_constants) as _,
        );

        let mut instance_base = 0;
        let mut instance_bytes_base = 0;
        for (model_handle, model_matrices) in model_matrices {
            let model = &allocated_models[&model_handle];

            let instance_bytes = model_matrices.len() * mem::size_of::<Mat4>();
            let instance_range = instance_bytes_base..instance_bytes_base + instance_bytes;
            bytemuck::cast_slice_mut(&mut in_flight.instance_buffer.slice_mut()[instance_range])
                .copy_from_slice(&model_matrices);

            ctx.device.cmd_bind_vertex_buffers(
                frame.cmd_buf(),
                0,
                &[
                    model.gpu_buffers.vertex_buffer.handle,
                    in_flight.instance_buffer.handle,
                ],
                &[0, instance_bytes_base as _],
            );

            ctx.device.cmd_bind_index_buffer(
                frame.cmd_buf(),
                model.gpu_buffers.index_buffer.handle,
                0,
                vk::IndexType::UINT32,
            );

            ctx.device.cmd_draw_indexed(
                frame.cmd_buf(),
                model.index_count,
                model_matrices.len() as _,
                0,
                0,
                0,
            );

            instance_bytes_base += instance_bytes;
            instance_base += model_matrices.len();
        }

        assert!(instance_base <= MAX_OBJECTS);
    }

    pub unsafe fn destroy(&mut self, ctx: &VulkanCtx) {
        for in_flight in &self.in_flight {
            in_flight.instance_buffer.destroy(ctx);
        }

        ctx.device.destroy_pipeline(self.pipeline, None);
        ctx.device
            .destroy_pipeline_layout(self.pipeline_layout, None);
    }
}
