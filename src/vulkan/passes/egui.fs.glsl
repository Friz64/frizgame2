#version 450
#extension GL_EXT_nonuniform_qualifier : enable

layout(location = 0) in vec4 inColor;
layout(location = 1) in vec2 inUV;

layout(location = 0) out vec4 outColor;

layout(push_constant) uniform PushConstants {
    layout(offset = 8) uint image;
};

layout(set = 0, binding = 0) uniform sampler2D images2D[];

void main() {
    outColor = inColor * texture(images2D[image], inUV).rrrr;
}
