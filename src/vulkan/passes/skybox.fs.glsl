#version 450
#extension GL_EXT_nonuniform_qualifier : enable

layout(location = 0) in vec3 inUVW;

layout(location = 0) out vec4 outColor;

layout(push_constant) uniform PushConstants {
    layout(offset = 64) uint image;
};

layout(set = 0, binding = 0) uniform samplerCube imagesCube[];

void main() {
    outColor = texture(imagesCube[image], inUVW);
}
