use bevy::prelude::*;
use erupt::vk;
use erupt_bootstrap::{DebugMessenger, InstanceBuilder, ValidationLayers};
use std::{
    ffi::{c_void, CStr},
    ptr,
};

const HIDDEN_VALIDATION: &[&str] = &[
    // Khronos Validation Layer Active
    "MessageID = 0xd7fa5f44",
    // VK_KHR_dynamic_rendering support is incomplete
    "MessageID = 0x79de34d4",
    // fragment shader writes to output location 0 with no matching attachment
    // https://github.com/KhronosGroup/Vulkan-ValidationLayers/issues/3554
    "MessageID = 0x609a13b",
];

unsafe extern "system" fn debug_callback(
    message_severity: vk::DebugUtilsMessageSeverityFlagBitsEXT,
    message_type: vk::DebugUtilsMessageTypeFlagsEXT,
    p_callback_data: *const vk::DebugUtilsMessengerCallbackDataEXT,
    _p_user_data: *mut c_void,
) -> vk::Bool32 {
    let message = CStr::from_ptr((*p_callback_data).p_message).to_string_lossy();
    let hidden = HIDDEN_VALIDATION
        .iter()
        .any(|entry| message.contains(entry));

    if !hidden {
        let message_severity = format!("{:?}", message_severity);
        let message_severity = message_severity.strip_suffix("_EXT").unwrap();

        let message_type = format!("{:?}", message_type);
        let message_type = message_type.strip_suffix("_EXT").unwrap();

        warn!("[{}: {}]\n{}", message_severity, message_type, message);
    }

    vk::FALSE
}

pub fn register(instance_builder: InstanceBuilder) -> InstanceBuilder {
    instance_builder
        .validation_layers(ValidationLayers::Request)
        .request_debug_messenger(DebugMessenger::Custom {
            callback: debug_callback,
            user_data_pointer: ptr::null_mut(),
        })
        .debug_message_severity(vk::DebugUtilsMessageSeverityFlagsEXT::all())
        .debug_message_type(
            vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION_EXT
                | vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE_EXT,
        )
}
