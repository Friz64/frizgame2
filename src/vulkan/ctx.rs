#[cfg(feature = "validation")]
mod validation;

use super::utils::{AllocatedBuffer, AllocatedImage, ImmutableModel, Ktx2Image};
use bevy::{asset::HandleId, prelude::*, utils::HashMap};
use erupt::{vk, DeviceLoader, EntryLoader, ExtendableFrom, InstanceLoader, SmallVec};
use erupt_bootstrap::{
    DeviceBuilder, DeviceMetadata, InstanceBuilder, InstanceMetadata, QueueFamilyCriteria,
};
use parking_lot::{Mutex, RwLock, RwLockReadGuard};
use std::{
    slice,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    thread::{self, ThreadId},
};
use vk_alloc::{Allocator, AllocatorDescriptor, MemoryLocation};
use winit::window::Window as WinitWindow;

const MAX_IMAGES: u32 = 256;

#[derive(Debug, Clone, Copy, Hash, Eq, PartialEq)]
pub enum Lifetime {
    EntireProgram,
    FreedRarely,
    FreedOnResize,
}

impl vk_alloc::Lifetime for Lifetime {}

#[derive(Clone)]
pub struct VulkanCtxContainer(Arc<(RwLock<VulkanCtx>, AtomicBool)>);

impl VulkanCtxContainer {
    pub fn get(&self) -> RwLockReadGuard<VulkanCtx> {
        if self.0 .1.load(Ordering::SeqCst) {
            // Context has been destroyed; Application will exit soon
            thread::park();
        }

        self.0 .0.read()
    }

    pub unsafe fn destroy(&self) {
        self.0 .1.store(true, Ordering::SeqCst);
        self.0 .0.write().destroy();
    }
}

pub struct QueueCtx {
    pub queue: vk::Queue,
    pub queue_family: u32,
    pub cmd_pool: CommandPools,
}

pub struct VulkanCtx {
    pub models: RwLock<HashMap<HandleId, ImmutableModel>>,
    pub images: RwLock<HashMap<HandleId, Ktx2Image>>,
    pub staging_buffers: StagingBuffers,
    pub sampler: vk::Sampler,
    pub image_set: ImageSet,
    pub fences: Fences,
    pub cmd_bufs: CommandBuffers,
    pub graphics_present_queue: QueueCtx,
    pub transfer_queue: QueueCtx,
    pub allocator: Allocator<Lifetime>,
    pub highest_sample_count: vk::SampleCountFlagBits,
    pub device_metadata: DeviceMetadata,
    pub physical_device: vk::PhysicalDevice,
    pub device: DeviceLoader,
    pub surface: vk::SurfaceKHR,
    pub _instance_metadata: InstanceMetadata,
    pub debug_messenger: Option<vk::DebugUtilsMessengerEXT>,
    pub instance: InstanceLoader,
    pub _entry: EntryLoader,
}

impl VulkanCtx {
    #[allow(clippy::new_ret_no_self)]
    pub unsafe fn new(winit_window: &WinitWindow) -> VulkanCtxContainer {
        let entry = EntryLoader::new().expect("Failed to load Vulkan entry functions");

        let mut instance_builder = InstanceBuilder::new();
        instance_builder = instance_builder
            .require_api_version(1, 2)
            .require_surface_extensions(winit_window)
            .expect("Failed to query required surface extensions");

        #[cfg(feature = "validation")]
        {
            instance_builder = validation::register(instance_builder);
        }

        let (instance, debug_messenger, instance_metadata) = instance_builder
            .build(&entry)
            .expect("Failed to create instance");

        let surface = erupt::utils::surface::create_surface(&instance, &winit_window, None)
            .expect("Failed to create surface");

        let mut sync2_features =
            vk::PhysicalDeviceSynchronization2FeaturesKHRBuilder::new().synchronization2(true);
        let mut dynamic_rendering_features =
            vk::PhysicalDeviceDynamicRenderingFeaturesKHRBuilder::new().dynamic_rendering(true);
        let mut vk1_2_features = vk::PhysicalDeviceVulkan12FeaturesBuilder::new()
            .descriptor_binding_partially_bound(true)
            .descriptor_binding_variable_descriptor_count(true)
            .descriptor_binding_sampled_image_update_after_bind(true)
            .runtime_descriptor_array(true);
        let device_features = vk::PhysicalDeviceFeatures2Builder::new()
            .extend_from(&mut sync2_features)
            .extend_from(&mut dynamic_rendering_features)
            .extend_from(&mut vk1_2_features);

        let graphics_present = QueueFamilyCriteria::graphics_present();
        let transfer = QueueFamilyCriteria::preferably_separate_transfer();
        let device_builder = DeviceBuilder::new()
            .queue_family(graphics_present)
            .queue_family(transfer)
            .require_version(1, 2)
            .require_extension(vk::KHR_SWAPCHAIN_EXTENSION_NAME)
            .require_extension(vk::KHR_SYNCHRONIZATION_2_EXTENSION_NAME)
            .require_extension(vk::KHR_DYNAMIC_RENDERING_EXTENSION_NAME)
            .require_features(&device_features)
            .for_surface(surface);
        let (device, device_metadata) = device_builder
            .build(&instance, &instance_metadata)
            .expect("Failed to create device");
        info!("Using GPU: {:?}", device_metadata.device_name());
        let physical_device = device_metadata.physical_device();

        let limits = device_metadata.properties().limits;
        let highest_sample_count = highest_sample_count(
            limits.framebuffer_color_sample_counts & limits.framebuffer_depth_sample_counts,
        );

        info!("Multisampling with {:?} samples", highest_sample_count);

        let allocator = Allocator::new(&instance, physical_device, &AllocatorDescriptor::default())
            .expect("Failed to create allocator");

        let queue_ctx = |requirements, cmd_pool_flags| {
            let data = device_metadata.device_queue(&instance, &device, requirements, 0);
            let (queue, queue_family) = data.unwrap().unwrap();
            let cmd_pool = CommandPools::new(queue_family, cmd_pool_flags);

            QueueCtx {
                queue,
                queue_family,
                cmd_pool,
            }
        };

        let sampler_info = vk::SamplerCreateInfoBuilder::new()
            .address_mode_u(vk::SamplerAddressMode::CLAMP_TO_EDGE)
            .address_mode_v(vk::SamplerAddressMode::CLAMP_TO_EDGE)
            .address_mode_w(vk::SamplerAddressMode::CLAMP_TO_EDGE)
            .anisotropy_enable(false)
            .min_filter(vk::Filter::LINEAR)
            .mag_filter(vk::Filter::LINEAR)
            .mipmap_mode(vk::SamplerMipmapMode::LINEAR)
            .min_lod(0.0)
            .max_lod(vk::LOD_CLAMP_NONE);
        let sampler = device.create_sampler(&sampler_info, None).unwrap();

        let ctx = VulkanCtx {
            models: RwLock::new(HashMap::default()),
            images: RwLock::new(HashMap::default()),
            staging_buffers: StagingBuffers::new(),
            sampler,
            image_set: ImageSet::new(&device),
            fences: Fences::new(),
            cmd_bufs: CommandBuffers::new(),
            graphics_present_queue: queue_ctx(
                graphics_present,
                vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER,
            ),
            transfer_queue: queue_ctx(transfer, vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER),
            allocator,
            highest_sample_count,
            device_metadata,
            physical_device,
            device,
            surface,
            _instance_metadata: instance_metadata,
            debug_messenger,
            instance,
            _entry: entry,
        };

        VulkanCtxContainer(Arc::new((RwLock::new(ctx), AtomicBool::new(false))))
    }

    unsafe fn destroy(&mut self) {
        for model in self.models.write().values_mut() {
            model.destroy(self);
        }

        for image in self.images.write().values_mut() {
            image.destroy(self);
        }

        self.staging_buffers.destroy(self);

        self.device.destroy_sampler(self.sampler, None);
        self.image_set.destroy(self);

        self.fences.destroy(self);

        self.graphics_present_queue.cmd_pool.destroy(self);
        self.transfer_queue.cmd_pool.destroy(self);

        self.allocator.cleanup(&self.device);
        self.device.destroy_device(None);

        self.instance.destroy_surface_khr(self.surface, None);

        if let Some(debug_messenger) = self.debug_messenger {
            self.instance
                .destroy_debug_utils_messenger_ext(debug_messenger, None);
        }

        self.instance.destroy_instance(None);
    }
}

fn highest_sample_count(flags: vk::SampleCountFlags) -> vk::SampleCountFlagBits {
    if flags.is_empty() {
        unreachable!();
    }

    let bits = flags.bits();
    vk::SampleCountFlagBits(if bits.is_power_of_two() {
        bits
    } else {
        bits.next_power_of_two() >> 1
    })
}

pub struct StagingBuffers(Mutex<SmallVec<AllocatedBuffer>>);

impl StagingBuffers {
    fn new() -> StagingBuffers {
        StagingBuffers(Mutex::new(SmallVec::new()))
    }

    pub unsafe fn retrieve(&self, ctx: &VulkanCtx, size: vk::DeviceSize) -> AllocatedBuffer {
        let mut buffers = self.0.lock();
        for (i, buffer) in buffers.iter().enumerate() {
            if buffer.size() >= size {
                return buffers.remove(i);
            }
        }

        AllocatedBuffer::new(
            ctx,
            size,
            vk::BufferUsageFlags::TRANSFER_SRC,
            MemoryLocation::CpuToGpu,
            Lifetime::EntireProgram,
        )
    }

    pub unsafe fn recycle(&self, buffer: AllocatedBuffer) {
        self.0.lock().push(buffer);
    }

    unsafe fn destroy(&self, ctx: &VulkanCtx) {
        for buffer in &*self.0.lock() {
            buffer.destroy(ctx);
        }
    }
}

pub struct Fences(Mutex<SmallVec<vk::Fence>>);

impl Fences {
    fn new() -> Fences {
        Fences(Mutex::new(SmallVec::new()))
    }

    pub unsafe fn retrieve(&self, ctx: &VulkanCtx) -> vk::Fence {
        let mut fences = self.0.lock();
        if let Some(fence) = fences.pop() {
            return fence;
        }

        let fence_info = vk::FenceCreateInfoBuilder::new();
        ctx.device.create_fence(&fence_info, None).unwrap()
    }

    pub fn recycle(&self, fence: vk::Fence) {
        self.0.lock().push(fence);
    }

    unsafe fn destroy(&self, ctx: &VulkanCtx) {
        for &fence in &*self.0.lock() {
            ctx.device.destroy_fence(fence, None);
        }
    }
}

pub struct CommandBuffers(Mutex<HashMap<vk::CommandPool, SmallVec<vk::CommandBuffer>>>);

impl CommandBuffers {
    fn new() -> CommandBuffers {
        CommandBuffers(Mutex::new(HashMap::default()))
    }

    pub unsafe fn retrieve(&self, ctx: &VulkanCtx, pool: vk::CommandPool) -> vk::CommandBuffer {
        let mut cmd_bufs = self.0.lock();
        if let Some(pool_bufs) = cmd_bufs.get_mut(&pool) {
            if let Some(cmd_buf) = pool_bufs.pop() {
                return cmd_buf;
            }
        }

        let cmd_buf_info = vk::CommandBufferAllocateInfoBuilder::new()
            .command_pool(pool)
            .level(vk::CommandBufferLevel::PRIMARY)
            .command_buffer_count(1);
        ctx.device.allocate_command_buffers(&cmd_buf_info).unwrap()[0]
    }

    pub fn recycle(&self, pool: vk::CommandPool, cmd_buf: vk::CommandBuffer) {
        self.0.lock().entry(pool).or_default().push(cmd_buf);
    }
}

pub struct CommandPools {
    pools: Mutex<HashMap<ThreadId, vk::CommandPool>>,
    flags: vk::CommandPoolCreateFlags,
    queue_family_index: u32,
}

impl CommandPools {
    fn new(queue_family_index: u32, flags: vk::CommandPoolCreateFlags) -> CommandPools {
        CommandPools {
            pools: Mutex::new(HashMap::default()),
            flags,
            queue_family_index,
        }
    }

    pub unsafe fn get(&self, ctx: &VulkanCtx) -> vk::CommandPool {
        let thread_id = thread::current().id();
        let mut pools = self.pools.lock();
        match pools.get(&thread_id) {
            Some(cmd_pool) => *cmd_pool,
            None => {
                let cmd_pool_info = vk::CommandPoolCreateInfoBuilder::new()
                    .flags(self.flags)
                    .queue_family_index(self.queue_family_index);
                let cmd_pool = ctx
                    .device
                    .create_command_pool(&cmd_pool_info, None)
                    .unwrap();

                *pools.entry(thread_id).or_default() = cmd_pool;
                cmd_pool
            }
        }
    }

    unsafe fn destroy(&self, ctx: &VulkanCtx) {
        for &cmd_pool in self.pools.lock().values() {
            ctx.device.destroy_command_pool(cmd_pool, None);
        }
    }
}

struct OccupiedBinding {
    image: AllocatedImage,
    in_use: bool,
}

pub struct ImageSet {
    descriptor_pool: vk::DescriptorPool,
    set_layout: vk::DescriptorSetLayout,
    descriptor_set: vk::DescriptorSet,
    occupied_bindings: Mutex<Vec<OccupiedBinding>>,
}

impl ImageSet {
    unsafe fn new(device: &DeviceLoader) -> ImageSet {
        let descriptor_pool = device
            .create_descriptor_pool(
                &vk::DescriptorPoolCreateInfoBuilder::new()
                    .flags(vk::DescriptorPoolCreateFlags::UPDATE_AFTER_BIND)
                    .max_sets(1)
                    .pool_sizes(&[vk::DescriptorPoolSizeBuilder::new()
                        ._type(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                        .descriptor_count(MAX_IMAGES)]),
                None,
            )
            .unwrap();

        let set_layout_bindings = vk::DescriptorSetLayoutBindingBuilder::new()
            .descriptor_type(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
            .descriptor_count(MAX_IMAGES)
            .binding(0)
            .stage_flags(vk::ShaderStageFlags::FRAGMENT);

        let binding_flags = vk::DescriptorBindingFlags::PARTIALLY_BOUND
            | vk::DescriptorBindingFlags::VARIABLE_DESCRIPTOR_COUNT
            | vk::DescriptorBindingFlags::UPDATE_AFTER_BIND;
        let mut binding_flags_info = vk::DescriptorSetLayoutBindingFlagsCreateInfoBuilder::new()
            .binding_flags(slice::from_ref(&binding_flags));

        let layout_info = vk::DescriptorSetLayoutCreateInfoBuilder::new()
            .bindings(slice::from_ref(&set_layout_bindings))
            .flags(vk::DescriptorSetLayoutCreateFlags::UPDATE_AFTER_BIND_POOL)
            .extend_from(&mut binding_flags_info);
        let set_layout = device
            .create_descriptor_set_layout(&layout_info, None)
            .unwrap();

        let mut variable_count_info =
            vk::DescriptorSetVariableDescriptorCountAllocateInfoBuilder::new()
                .descriptor_counts(slice::from_ref(&MAX_IMAGES));

        let allocate_info = vk::DescriptorSetAllocateInfoBuilder::new()
            .descriptor_pool(descriptor_pool)
            .set_layouts(slice::from_ref(&set_layout))
            .extend_from(&mut variable_count_info);
        let descriptor_set = device.allocate_descriptor_sets(&allocate_info).unwrap()[0];

        ImageSet {
            descriptor_pool,
            set_layout,
            descriptor_set,
            occupied_bindings: Mutex::new(Default::default()),
        }
    }

    pub unsafe fn retrieve(
        &self,
        ctx: &VulkanCtx,
        image: AllocatedImage,
        image_layout: vk::ImageLayout,
    ) -> BoundImage {
        let mut occupied_bindings = self.occupied_bindings.lock();
        let update = |index| {
            ctx.device.update_descriptor_sets(
                &[vk::WriteDescriptorSetBuilder::new()
                    .descriptor_type(vk::DescriptorType::COMBINED_IMAGE_SAMPLER)
                    .dst_set(ctx.image_set.descriptor_set())
                    .dst_binding(0)
                    .dst_array_element(index)
                    .image_info(&[vk::DescriptorImageInfoBuilder::new()
                        .image_view(image.view())
                        .image_layout(image_layout)
                        .sampler(ctx.sampler)])],
                &[],
            )
        };

        match occupied_bindings
            .iter_mut()
            .enumerate()
            .find(|(_i, binding)| !binding.in_use)
        {
            Some((i, binding)) => {
                let index = i as u32;
                update(index);
                binding.image.destroy(ctx);
                binding.image = image;
                binding.in_use = true;
                BoundImage { index }
            }
            None => {
                let index = occupied_bindings.len() as u32;
                update(index);
                occupied_bindings.push(OccupiedBinding {
                    image,
                    in_use: true,
                });

                BoundImage { index }
            }
        }
    }

    pub fn access<T>(&self, image: BoundImage, access: impl FnOnce(&AllocatedImage) -> T) -> T {
        let occupied_bindings = self.occupied_bindings.lock();
        access(&occupied_bindings[image.index as usize].image)
    }

    pub fn release(&self, image: BoundImage) {
        let mut occupied_bindings = self.occupied_bindings.lock();
        occupied_bindings[image.index as usize].in_use = false;
    }

    pub fn descriptor_set(&self) -> vk::DescriptorSet {
        self.descriptor_set
    }

    pub fn set_layout(&self) -> vk::DescriptorSetLayout {
        self.set_layout
    }

    unsafe fn destroy(&self, ctx: &VulkanCtx) {
        for binding in self.occupied_bindings.lock().iter() {
            binding.image.destroy(ctx);
        }

        ctx.device
            .destroy_descriptor_set_layout(self.set_layout, None);
        ctx.device
            .destroy_descriptor_pool(self.descriptor_pool, None);
    }
}

#[derive(Clone, Copy)]
#[repr(transparent)]
pub struct BoundImage {
    index: u32,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_highest_sample_count() {
        assert_eq!(
            highest_sample_count(vk::SampleCountFlags::_2),
            vk::SampleCountFlagBits::_2
        );

        assert_eq!(
            highest_sample_count(vk::SampleCountFlags::_1 | vk::SampleCountFlags::_2),
            vk::SampleCountFlagBits::_2
        );

        assert_eq!(
            highest_sample_count(vk::SampleCountFlags::_1 | vk::SampleCountFlags::_4),
            vk::SampleCountFlagBits::_4
        );
    }
}
