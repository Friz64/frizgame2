use super::{
    ctx::VulkanCtxContainer,
    transfer::TransferOp,
    utils::{Barrier, ImmutableModel, Ktx2Image},
};
use crate::model::ModelDesc;
use bevy::asset::HandleId;
use erupt::{vk, SmallVec};
use parking_lot::Mutex;
use std::{borrow::Cow, sync::Arc};

#[derive(Clone)]
pub struct AssetInterface {
    pub ctx: VulkanCtxContainer,
    dst_barriers: Arc<Mutex<SmallVec<Barrier>>>,
}

impl AssetInterface {
    pub(super) fn new(ctx: VulkanCtxContainer) -> AssetInterface {
        AssetInterface {
            ctx,
            dst_barriers: Arc::new(Mutex::new(SmallVec::new())),
        }
    }

    pub unsafe fn upload_model(&self, handle_id: HandleId, model_desc: ModelDesc) {
        let ctx = self.ctx.get();
        let mut transfer = TransferOp::new(&ctx);
        let mut allocated = ImmutableModel::new(&ctx, &mut transfer, &model_desc);
        self.dst_barriers.lock().extend(transfer.finish(&ctx));
        allocated.recycle_staging(&ctx);
        ctx.models.write().insert(handle_id, allocated);
    }

    pub unsafe fn upload_ktx2(
        &self,
        handle_id: HandleId,
        header: ktx2::Header,
        levels: SmallVec<Cow<[u8]>>,
    ) {
        let ctx = self.ctx.get();
        let mut transfer = TransferOp::new(&ctx);
        let mut allocated = Ktx2Image::new(&ctx, &mut transfer, header, levels);
        self.dst_barriers.lock().extend(transfer.finish(&ctx));
        allocated.recycle_staging(&ctx);
        ctx.images.write().insert(handle_id, allocated);
    }

    pub(super) unsafe fn acquire_barrier(&self, cmd_buf: vk::CommandBuffer) {
        let mut barriers = self.dst_barriers.lock();
        if !barriers.is_empty() {
            let ctx = self.ctx.get();
            Barrier::submit_barriers(&ctx, cmd_buf, barriers.drain(..));
        }
    }
}
