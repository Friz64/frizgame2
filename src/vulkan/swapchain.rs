use super::{ctx::VulkanCtx, ImageWithView, FRAMES_IN_FLIGHT};
use bevy::{prelude::*, window::PresentMode};
use erupt::{vk, SmallVec};
use std::slice;
use winit::{dpi::PhysicalSize, window::Window as WinitWindow};

const FORMAT_CANDIDATES: &[vk::Format] = &[
    vk::Format::R8G8B8A8_UNORM,
    vk::Format::B8G8R8A8_UNORM,
    vk::Format::A8B8G8R8_UNORM_PACK32,
];
const PRESENT_MODE_CANDIDATES: &[vk::PresentModeKHR] = &[
    vk::PresentModeKHR::MAILBOX_KHR,
    vk::PresentModeKHR::IMMEDIATE_KHR,
];
const COMPOSITE_ALPHA_CANDIDATES: &[vk::CompositeAlphaFlagBitsKHR] = &[
    vk::CompositeAlphaFlagBitsKHR::OPAQUE_KHR,
    vk::CompositeAlphaFlagBitsKHR::INHERIT_KHR,
    vk::CompositeAlphaFlagBitsKHR::PRE_MULTIPLIED_KHR,
    vk::CompositeAlphaFlagBitsKHR::POST_MULTIPLIED_KHR,
];

pub const SUBRESOURCE_RANGE: vk::ImageSubresourceRange = vk::ImageSubresourceRange {
    aspect_mask: vk::ImageAspectFlags::COLOR,
    base_mip_level: 0,
    level_count: 1,
    base_array_layer: 0,
    layer_count: 1,
};

pub struct Frame {
    pub extent: vk::Extent2D,
    in_flight: InFlight,
    pub image_idx: u32,
    pub image_format: vk::SurfaceFormatKHR,
    pub image: ImageWithView,
}

impl Frame {
    pub fn in_flight_idx(&self) -> usize {
        self.in_flight.idx
    }

    pub fn cmd_buf(&self) -> vk::CommandBuffer {
        self.in_flight.cmd_buf
    }
}

#[derive(Clone)]
pub struct InFlight {
    idx: usize,
    cmd_buf: vk::CommandBuffer,
    acquire_semaphore: vk::Semaphore,
    complete_fence: vk::Fence,
    complete_semaphore: vk::Semaphore,
}

pub struct Swapchain {
    handle: vk::SwapchainKHR,
    format_pair: vk::SurfaceFormatKHR,
    extent: vk::Extent2D,
    present_mode: Option<vk::PresentModeKHR>,
    recreate_requested: bool,
    images: SmallVec<ImageWithView>,
    in_flight: SmallVec<InFlight>,
    vsync: bool,
}

impl Swapchain {
    pub unsafe fn new(ctx: &VulkanCtx) -> Swapchain {
        let cmd_buf_info = vk::CommandBufferAllocateInfoBuilder::new()
            .command_pool(ctx.graphics_present_queue.cmd_pool.get(ctx))
            .level(vk::CommandBufferLevel::PRIMARY)
            .command_buffer_count(FRAMES_IN_FLIGHT as _);
        let semaphore_info = vk::SemaphoreCreateInfoBuilder::new();
        let fence_info = vk::FenceCreateInfoBuilder::new().flags(vk::FenceCreateFlags::SIGNALED);
        let frames_in_flight = ctx
            .device
            .allocate_command_buffers(&cmd_buf_info)
            .unwrap()
            .into_iter()
            .enumerate()
            .map(|(idx, cmd_buf)| InFlight {
                idx,
                cmd_buf,
                acquire_semaphore: ctx.device.create_semaphore(&semaphore_info, None).unwrap(),
                complete_fence: ctx.device.create_fence(&fence_info, None).unwrap(),
                complete_semaphore: ctx.device.create_semaphore(&semaphore_info, None).unwrap(),
            })
            .collect();

        let surface_formats = ctx
            .instance
            .get_physical_device_surface_formats_khr(ctx.physical_device, ctx.surface, None)
            .unwrap();

        let format_pair = match *surface_formats.as_slice() {
            [single] if single.format == vk::Format::UNDEFINED => vk::SurfaceFormatKHR {
                format: vk::Format::B8G8R8A8_UNORM,
                color_space: single.color_space,
            },
            _ => *surface_formats
                .iter()
                .find(|surface_format| FORMAT_CANDIDATES.contains(&surface_format.format))
                .unwrap_or(&surface_formats[0]),
        };

        Swapchain {
            handle: vk::SwapchainKHR::null(),
            format_pair,
            extent: vk::Extent2D::default(),
            present_mode: None,
            recreate_requested: true,
            images: SmallVec::new(),
            in_flight: frames_in_flight,
            vsync: true,
        }
    }

    unsafe fn recreate(&mut self, ctx: &VulkanCtx, winit_window: &WinitWindow, vsync: bool) {
        self.vsync = vsync;
        self.recreate_requested = false;
        ctx.device
            .queue_wait_idle(ctx.graphics_present_queue.queue)
            .unwrap();

        let surface_caps = ctx
            .instance
            .get_physical_device_surface_capabilities_khr(ctx.physical_device, ctx.surface)
            .unwrap();
        let surface_present_modes = ctx
            .instance
            .get_physical_device_surface_present_modes_khr(ctx.physical_device, ctx.surface, None)
            .unwrap();

        let swapchain_extent = if surface_caps.current_extent.width == u32::MAX {
            let PhysicalSize { width, height } = winit_window.inner_size();
            vk::Extent2D { width, height }
        } else {
            surface_caps.current_extent
        };

        let mut present_mode = vk::PresentModeKHR::FIFO_KHR;
        if !vsync {
            if let Some(val) = PRESENT_MODE_CANDIDATES
                .iter()
                .find(|candidate| surface_present_modes.contains(candidate))
            {
                present_mode = *val;
            }
        }

        let mut min_swapchain_image_count = surface_caps.min_image_count.max(3);
        if surface_caps.max_image_count > 0 {
            min_swapchain_image_count = min_swapchain_image_count.min(surface_caps.max_image_count);
        }

        let pre_transform = if surface_caps
            .supported_transforms
            .contains(vk::SurfaceTransformFlagsKHR::IDENTITY_KHR)
        {
            vk::SurfaceTransformFlagBitsKHR::IDENTITY_KHR
        } else {
            surface_caps.current_transform
        };

        let composite_alpha = COMPOSITE_ALPHA_CANDIDATES
            .iter()
            .copied()
            .find(|candidate| {
                surface_caps
                    .supported_composite_alpha
                    .contains(candidate.bitmask())
            })
            .unwrap_or(vk::CompositeAlphaFlagBitsKHR::OPAQUE_KHR);

        let swapchain_info = vk::SwapchainCreateInfoKHRBuilder::new()
            .surface(ctx.surface)
            .min_image_count(min_swapchain_image_count)
            .image_format(self.format_pair.format)
            .image_color_space(self.format_pair.color_space)
            .image_extent(swapchain_extent)
            .image_array_layers(1)
            .image_usage(vk::ImageUsageFlags::COLOR_ATTACHMENT)
            .image_sharing_mode(vk::SharingMode::EXCLUSIVE)
            .pre_transform(pre_transform)
            .composite_alpha(composite_alpha)
            .present_mode(present_mode)
            .clipped(true)
            .old_swapchain(self.handle);
        let new_swapchain = ctx
            .device
            .create_swapchain_khr(&swapchain_info, None)
            .expect("Failed to create swapchain");

        self.destroy_swapchain(ctx);
        self.handle = new_swapchain;
        self.extent = swapchain_extent;
        self.present_mode = Some(present_mode);

        info!(
            "Swapchain created @ {}x{}, present mode {:?}",
            swapchain_extent.width, swapchain_extent.height, present_mode
        );

        self.images = ctx
            .device
            .get_swapchain_images_khr(new_swapchain, None)
            .unwrap()
            .into_iter()
            .map(|image| {
                let image_view_info = vk::ImageViewCreateInfoBuilder::new()
                    .view_type(vk::ImageViewType::_2D)
                    .format(self.format_pair.format)
                    .image(image)
                    .subresource_range(SUBRESOURCE_RANGE)
                    .components(vk::ComponentMapping {
                        r: vk::ComponentSwizzle::R,
                        g: vk::ComponentSwizzle::G,
                        b: vk::ComponentSwizzle::B,
                        a: vk::ComponentSwizzle::A,
                    });

                ImageWithView {
                    handle: image,
                    view: ctx
                        .device
                        .create_image_view(&image_view_info, None)
                        .unwrap(),
                }
            })
            .collect();
    }

    pub fn format_pair(&self) -> vk::SurfaceFormatKHR {
        self.format_pair
    }

    pub fn present_mode(&self) -> vk::PresentModeKHR {
        self.present_mode.unwrap()
    }

    pub unsafe fn acquire_frame(
        &mut self,
        ctx: &VulkanCtx,
        frame_idx: usize,
        window: &Window,
        winit_window: &WinitWindow,
    ) -> Frame {
        let vsync = window.present_mode() == PresentMode::Fifo;
        if self.recreate_requested || self.vsync != vsync {
            self.recreate(ctx, winit_window, vsync);
        }

        let in_flight = self.in_flight[frame_idx].clone();

        ctx.device
            .wait_for_fences(slice::from_ref(&in_flight.complete_fence), true, u64::MAX)
            .unwrap();
        ctx.device
            .reset_fences(slice::from_ref(&in_flight.complete_fence))
            .unwrap();

        let image_idx = loop {
            let result = ctx.device.acquire_next_image_khr(
                self.handle,
                u64::MAX,
                in_flight.acquire_semaphore,
                vk::Fence::null(),
            );

            if matches!(result.raw, vk::Result::ERROR_OUT_OF_DATE_KHR) {
                self.recreate(ctx, winit_window, vsync);
            } else if let Some(index) = result.value {
                if matches!(result.raw, vk::Result::SUBOPTIMAL_KHR) {
                    self.recreate_requested = true;
                }

                break index;
            } else {
                result.expect("Failed to acquire swapchain image");
            }
        };

        let cmd_buf_info = vk::CommandBufferBeginInfoBuilder::new()
            .flags(vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT);
        ctx.device
            .begin_command_buffer(in_flight.cmd_buf, &cmd_buf_info)
            .unwrap();

        Frame {
            extent: self.extent,
            in_flight,
            image_idx,
            image_format: self.format_pair,
            image: self.images[image_idx as usize],
        }
    }

    pub unsafe fn present_frame(&self, ctx: &VulkanCtx, frame: Frame) {
        ctx.device.end_command_buffer(frame.cmd_buf()).unwrap();

        let acquire_wait = vk::SemaphoreSubmitInfoKHRBuilder::new()
            .semaphore(frame.in_flight.acquire_semaphore)
            .stage_mask(vk::PipelineStageFlags2KHR::COLOR_ATTACHMENT_OUTPUT_KHR);
        let cmd_buf_submit_info =
            vk::CommandBufferSubmitInfoKHRBuilder::new().command_buffer(frame.cmd_buf());
        let complete_signal = vk::SemaphoreSubmitInfoKHRBuilder::new()
            .semaphore(frame.in_flight.complete_semaphore)
            .stage_mask(vk::PipelineStageFlags2KHR::COLOR_ATTACHMENT_OUTPUT_KHR);
        let submit_info = vk::SubmitInfo2KHRBuilder::new()
            .wait_semaphore_infos(slice::from_ref(&acquire_wait))
            .command_buffer_infos(slice::from_ref(&cmd_buf_submit_info))
            .signal_semaphore_infos(slice::from_ref(&complete_signal));

        ctx.device
            .queue_submit2_khr(
                ctx.graphics_present_queue.queue,
                &[submit_info],
                frame.in_flight.complete_fence,
            )
            .unwrap();

        let present_info = vk::PresentInfoKHRBuilder::new()
            .wait_semaphores(slice::from_ref(&frame.in_flight.complete_semaphore))
            .swapchains(slice::from_ref(&self.handle))
            .image_indices(slice::from_ref(&frame.image_idx));

        let result = ctx
            .device
            .queue_present_khr(ctx.graphics_present_queue.queue, &present_info);
        if result.is_err() && result.raw != vk::Result::ERROR_OUT_OF_DATE_KHR {
            panic!("Failed to queue image for presentation");
        }
    }

    unsafe fn destroy_swapchain(&self, ctx: &VulkanCtx) {
        for ImageWithView { view, .. } in &self.images {
            ctx.device.destroy_image_view(*view, None);
        }

        ctx.device.destroy_swapchain_khr(self.handle, None);
    }

    pub unsafe fn destroy(&self, ctx: &VulkanCtx) {
        for frame in &self.in_flight {
            ctx.device.destroy_semaphore(frame.acquire_semaphore, None);
            ctx.device.destroy_fence(frame.complete_fence, None);
            ctx.device.destroy_semaphore(frame.complete_semaphore, None);
        }

        self.destroy_swapchain(ctx);
    }
}
