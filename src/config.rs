use bevy::{
    prelude::*,
    window::{PresentMode, WindowMode},
};
use egui::Widget;
use ron::ser::PrettyConfig;
use serde::{Deserialize, Serialize};
use std::{
    fs::{self, File},
    path::Path,
};

const CONFIG_NAME: &str = "frizgame2.ron";

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    pub vsync: bool,
    pub frame_rate_limit: f32,
    pub fullscreen: bool,
    pub fov: f32,
    pub mouse_sensitivity: f32,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            vsync: true,
            frame_rate_limit: f32::INFINITY,
            fullscreen: false,
            fov: 90.0,
            mouse_sensitivity: 1.0,
        }
    }
}

impl Config {
    fn load() -> Config {
        let path = Path::new(CONFIG_NAME);

        let config;
        if path.exists() {
            let file = File::open(path).expect("Failed to open config file");
            match ron::de::from_reader(file) {
                Ok(val) => config = val,
                Err(err) => panic!("Failed to parse config: {err}"),
            }

            info!("Successfully read config");
        } else {
            warn!("Config not found, saving default");
            config = Config::default();
            config.save();
        }

        config
    }

    fn save(&self) {
        info!("Saving config");
        let pretty = ron::ser::to_string_pretty(&self, PrettyConfig::new())
            .expect("Failed to serialize config");
        fs::write(CONFIG_NAME, pretty).expect("Failed to write config file");
    }
}

impl Drop for Config {
    fn drop(&mut self) {
        self.save();
    }
}

pub struct ConfigPlugin;

impl Plugin for ConfigPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(Config::load())
            .add_system(apply_config.label(ApplyConfigSystem));
    }
}

#[derive(Clone, Hash, Debug, PartialEq, Eq, SystemLabel)]
pub struct ApplyConfigSystem;

#[derive(Default)]
struct ApplyConfigState {
    applied: bool,
    prev_vsync: bool,
    prev_fullscreen: bool,
}

fn apply_config(
    mut state: Local<ApplyConfigState>,
    config: Res<Config>,
    mut windows: ResMut<Windows>,
) {
    let window = windows.get_primary_mut().unwrap();

    if !state.applied || state.prev_vsync != config.vsync {
        state.prev_vsync = config.vsync;
        window.set_present_mode(if config.vsync {
            PresentMode::Fifo
        } else {
            PresentMode::Mailbox
        });
    }

    if !state.applied || state.prev_fullscreen != config.fullscreen {
        state.prev_fullscreen = config.fullscreen;
        window.set_mode(if config.fullscreen {
            WindowMode::BorderlessFullscreen
        } else {
            WindowMode::Windowed
        });
    }

    state.applied = true;
}

pub fn ui(ui: &mut egui::Ui, config: &mut Config) {
    egui::CollapsingHeader::new("🔧 Config").show(ui, |ui| {
        ui.checkbox(&mut config.vsync, "VSync");
        egui::Slider::new(&mut config.frame_rate_limit, 30.0..=f32::INFINITY)
            .logarithmic(true)
            .integer()
            .largest_finite(300.0)
            .text("Frame rate limit")
            .ui(ui);
        ui.checkbox(&mut config.fullscreen, "Fullscreen");
        egui::Slider::new(&mut config.fov, 30.0..=130.0)
            .integer()
            .text("FOV")
            .ui(ui);
        egui::Slider::new(&mut config.mouse_sensitivity, 0.2..=5.0)
            .max_decimals(1)
            .text("Mouse Sensitivity")
            .ui(ui);
    });
}
