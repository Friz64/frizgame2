use std::process::Command;

fn main() {
    let build_date = stdout("date", &["-u", "+%Y-%m-%d %H:%M UTC"]);
    println!("cargo:rustc-env=FG2_BUILD_DATE={build_date}");

    let git_rev = stdout("git", &["describe", "--always", "--dirty=-dirty"]);
    println!("cargo:rustc-env=FG2_GIT_REV={git_rev}");
}

fn stdout(program: &str, args: &[&str]) -> String {
    let output = Command::new(program).args(args).output().unwrap();
    String::from_utf8(output.stdout).unwrap()
}
