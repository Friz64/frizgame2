<img src="logo.png" height=200>

# frizgame2

Yet another game project by Friz64.

An up-to-date Vulkan 1.2 driver supporting `VK_KHR_dynamic_rendering` and
`VK_KHR_synchronization2` is required. The target operating systems are Linux
and Windows.

Download the latest build for Linux
[here](https://gitlab.com/Friz64/frizgame2/-/jobs/artifacts/main/download?job=cargo)
or compile & run an development build with `./dev.sh`.

## Project Acknowledgements

- [bevy](https://github.com/bevyengine/bevy)
- [vk-alloc](https://github.com/hasenbanck/vk-alloc)
- [egui-winit-ash-integration](https://github.com/MatchaChoco010/egui-winit-ash-integration)
- [egui](https://docs.rs/egui/)
- [rapier](https://rapier.rs/)
- [rhai](https://rhai.rs/)

## Licensing

This project is licensed under the [zlib License](LICENSE).
